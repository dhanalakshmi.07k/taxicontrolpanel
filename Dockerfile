FROM registry.gitlab.com/nec/necbase
MAINTAINER Ritesh Ranjan

# Application specific configuration

ENV APP_INSTALL_PATH /tfmCP
WORKDIR $APP_INSTALL_PATH

RUN mkdir -p $APP_INSTALL_PATH

COPY package.json .
RUN npm install

COPY app ./app

#RUN node app/generator/settingsGenerator.js \
#    && node app/generator/mapPlotGenerator.js \
#    && node app/generator/userCredencials.js

COPY config ./config
COPY app.js Gruntfile.js ./
COPY Public ./Public


ENV NODE_ENV=docker

EXPOSE 5200
CMD node app.js


