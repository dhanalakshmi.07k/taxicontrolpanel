/**
 * Created by zendynamix on 22-07-2016.
 */
taxiFleetControlPanel.service('routeService',['$http', function($http) {
    var routeConfig={};
    var pulseApplicationConstants={};
    var routeService = {
        getPulseApiConstantDetails: function () {
            return $http.get("/pulseApiBaseUrls");
        },
        setPulseApiConstants:function (pulseAppConfig) {
            pulseApplicationConstants = pulseAppConfig;
        },
        getPulseApiConstants: function () {
            return pulseApplicationConstants;
        }

    }
    return routeService;


}]);
