/**
 * Created by zendynamix on 7/8/2016.
 */
taxiFleetControlPanel.service('settingService',['$http', function($http) {
    var mapObj={};
    var appSettings=null;
    var settingService = {
        addMapConfigDetails: function (mapConfigObject) {
            return $http.post('/config',mapConfigObject);
        },
        getMapConfig:function(){
            return $http.get('/configMapDetails');
        },

        init:function(){
            var p =$http.get('/appSettings')
            p.then(function(res) {
                appSettings= res.data;
            })
            return p;
        },
        getAppSettings:function(){
            return appSettings
        },
        setMapConfigObj:function(mapConfigObj){
            mapObj=mapConfigObj;

        },
        getMapConfigObj:function(){
            return mapObj;
        }

    }

    return settingService;



}]);
