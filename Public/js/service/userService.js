/**
 * Created by Suhas on 3/8/2016.
 */
taxiFleetControlPanel.factory("userService", function ($http) {
        var createNewUser = function(userData){
                return $http.post('/signup',userData)
        }
        var getAllUsers = function(){
                return $http.get('/necControlPanel/userDetails/getAll');
        }
        var deleteUser = function(id){
                return $http.get('/necControlPanel/userDetails/delete/'+id);
        }
        var getUserCount = function(){
                return $http.get('/necControlPanel/userDetails/count');
        }
        var getUserDetailsByRange = function(skip,limit){
                return $http.get('/necControlPanel/userDetails/get/'+skip+'/'+limit)
        }
        var updateUserDetails = function(userData){
                return $http.post('/necControlPanel/userDetails/update',userData)
        }
        var isUserNameUnique = function(userName){
                return $http.get('/necControlPanel/userDetails/isUserNameUnique/'+userName)
        }
        return{
                createNewUser:createNewUser,
                getAllUsers:getAllUsers,
                deleteUser:deleteUser,
                getUserCount:getUserCount,
                getUserDetailsByRange:getUserDetailsByRange,
                updateUserDetails:updateUserDetails,
                isUserNameUnique:isUserNameUnique
        }
})