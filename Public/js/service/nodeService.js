/**
 * Created by zendynamix on 14-06-2016.
 */
taxiFleetControlPanel.service('nodeService',['$http','appService', function($http,appService) {

    var pulseAppBaseUrl="/pulseWatchList";
    var nodeService = {
        getAllNodes: function () {
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.nodeApiUrl);
        }

    }

    return nodeService;



}]);
