/**
 * Created by Suhas on 4/5/2016.
 */
taxiFleetControlPanel.factory('watchListService',function($http){
        var startWatchList = function(){
                return $http.get('/necDashBoard/watchList/start')
        }
        var stopWatchList = function(){
                return $http.get('/necDashBoard/watchList/stop')
        }
        var getWatchListStatus = function(){
                return $http.get('/necDashBoard/settings/getTopologyInstanceState')
        }
        var getWatchListPersonId=function(peronWatchListUrl,config){
                return $http.post(peronWatchListUrl,config);

        }
        var addPersonToWatchListById=function(url,personDetails,fileObj){
                console.log(url)
                console.log(personDetails)
                console.log(fileObj)
                var fd = new FormData();
                fd.append('file', fileObj);
                console.log(fd)
                return $http.post(url+personDetails.personId+"/pictures?retainPicture="+personDetails.retainPictureStatus+"&matchThreshold="+personDetails.matchThreshold,fd,{
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                })

        }

        return{
                stopWatchList:stopWatchList,
                startWatchList:startWatchList,
                getWatchListStatus:getWatchListStatus,
                getWatchListPersonId:getWatchListPersonId,
                addPersonToWatchListById:addPersonToWatchListById

        }
})