
/**
 * Created by zendynamix on 8/11/2016.
 */


taxiFleetControlPanel.factory('assetApiService',function($http,assetApiUrlService,sensorService){
    var getDevicesDetails= function(){
        return assetApiUrlService.getDevices().then(function (result) {
            var deviceDetailsArray=[];
            for(var l=0;l<result.data[0].items.length;l++){
                var detailsObj={};
                detailsObj.id=result.data[0].items[l].id
                detailsObj.name=result.data[0].items[l].name
                detailsObj.online=result.data[0].items[l].online
                detailsObj.lat=result.data[0].items[l].lat
                detailsObj.lng=result.data[0].items[l].lng
                detailsObj.createdDate=result.data[0].items[l].device_data.created_at
                detailsObj.updatedDate=result.data[0].items[l].device_data.updated_at
                deviceDetailsArray.push(detailsObj);
                if(result.data[0].items.length-1==l) {
                    return deviceDetailsArray;
                }
            }
             }, function error(errResponse) {
                     return errResponse;
                 });
    }
    var getDeviceHistory = function(deviceId){
        /*var fromDate=moment().format('YYYY-MM-DD')
         var toDate=moment().format('YYYY-MM-DD')
         var fromTime=moment().format('HH:mm');
         var toTime=moment().subtract(1,'hour').format('HH:mm');*/

        var fromDate='2016-08-15'
        var toDate='2016-08-15'
        var fromTime='00:00';
        var toTime='24:00';
        return $http.get('/getDeviceHistoryById/'+fromDate+'/'+toDate+'/'+fromTime+'/'+toTime+'/'+ deviceId)
    }
    var addDevice = function(deviceObj){
        return $http.post('/addDevice',deviceObj);
    }
    var deleteDevice = function(deviceApiRefId){
        return $http.get('/deleteDevice/'+deviceApiRefId);
    }
    var editDevice = function(){

    }
    var getMapPlotsFromApi=function(){
     return getDevicesFromApiAndLocal();
    }
    function getDevicesFromApiAndLocal(){

        return  assetApiUrlService.getDevices().then(function (result) {
            return   sensorService.getAllAssetsDetailsByType('obdDevice').then(function (response) {
                console.log("inside ")
                console.log(response.data)
                console.log(result.data[0].items)

                var deviceDetailsArray=[];
                for(var  i=0;i<response.data.length;i++){
                    for(var  j=0;j<result.data[0].items.length;j++){
                        if(response.data[i].sensorData.uniqueIdentifier==result.data[0].items[j].device_data.imei){
                            var detailsObj={};
                            detailsObj.id=result.data[0].items[j].id
                            detailsObj.name=result.data[0].items[j].name
                            detailsObj.online=result.data[0].items[j].online
                            detailsObj.lat=result.data[0].items[j].lat
                            detailsObj.lng=result.data[0].items[j].lng
                            detailsObj.tail=result.data[0].items[j].tail
                            deviceDetailsArray.push(detailsObj);
                        }
                    }

                    if(response.data.length-1===i) {
                        return (deviceDetailsArray.length>0?deviceDetailsArray:[]);
                    }

                }


            }, function error(errResponse) {
                return errResponse;
            });

        }, function error(errResponse) {
            return errResponse;
        });

    }
    return{
        getDevicesDetails:getDevicesDetails,
        getDeviceHistory:getDeviceHistory,
        addDevice:addDevice,
        deleteDevice:deleteDevice,
        editDevice:editDevice,
        getMapPlotsFromApi:getMapPlotsFromApi


    }
})


/*
function getMatch(a, b) {
    var matches = [];

    for ( var i = 0; i < a.length; i++ ) {
        for ( var e = 0; e < b.length; e++ ) {
            if ( a[i] === b[e] ) matches.push( a[i] );
        }
    }
    alert(matches);
}*/
