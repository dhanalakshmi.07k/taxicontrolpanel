/**
 * Created by Suhas on 4/2/2016.
 */
taxiFleetControlPanel.factory("notificationService", function ($http,$rootScope) {
        var notificationObj = {}
        const notificationConstants = {
                "NOTIFICATION_DATA_NOTIFIER":"notificationNotifier"
        }
        socket.on('notificationNotifier',function (data) {
                console.log(data)
                notificationObj=data;
                triggerNotification(data);
        });
        var getNotification = function(){
                return notificationObj;
        }
        var triggerNotification = function(data){
                $rootScope.$emit(notificationConstants.NOTIFICATION_DATA_NOTIFIER,data);
        }
        return{
                getNotification:getNotification,
                notificationConstants:notificationConstants
        }
})