/**
 * Created by Suhas on 8/11/2016.
 */

taxiFleetControlPanel.factory("linkAssetsService", function ($http) {
        var linkAssets = function(linkAssetsDetails){
                return $http.put('/api/linkAssets',linkAssetsDetails);
        }
        var deLinkAssets = function(linkAssetsDetails){
                return $http.put('/api/deLinkAssets',linkAssetsDetails);
        }
        var getLinkedAssetsCount = function(assetType){
                return $http.get('/api/linkedAssetsCounts/'+assetType);
        }
        var getLinkedAssetsLists = function(skip,limit,assetType){
                return $http.get('/api/linkedAssets/'+skip+"/"+limit+'/'+assetType);
        }
        return {
                linkAssets:linkAssets,
                getLinkedAssetsCount:getLinkedAssetsCount,
                getLinkedAssetsLists:getLinkedAssetsLists,
                deLinkAssets:deLinkAssets
        }
})