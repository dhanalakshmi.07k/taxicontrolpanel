/**
 * Created by zendynamix on 07-06-2016.
 */


taxiFleetControlPanel.service('appService',['$http', function($http) {
       /* var routeConfig={};*/
        var pulseApplicationConstants={};
        var applicationConstants={};
        var appConfigMapperConstants=[];
        var appInstanceDetails;
        var pulseAppBaseUrl="/pulseWatchList";

    var appService = {

        getAppDetails: function () {
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl);
        },

        deleteAppById: function ( appId) {
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.delete(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl+'/' + appId);
        },
        getAppDetailsByID: function (appId) {
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl+'/' + appId);
        },
        getAppSampleJson: function (appId) {
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl+'/'+appId+"/configurationSample");
        },

        getAppConfigurationById:function(appId){
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl+'/'+appId+"/configurationSample");
        },
        setApplicationConstants:function (appconfig) {
            applicationConstants = appconfig;
        },
        getApplicationConstants: function () {
            return applicationConstants;
        },
        getConstantDetails: function () {
            return $http.get("/constants");
        },

        appApplication: function (fileObj) {
            var fd = new FormData();
            fd.append('file', fileObj);
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.post(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl,fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
           /* var fd = new FormData();
            fd.append('file', fileObj);
            return $http.post(uploadUrl,fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })*/
        },
        getAppDetailsFromDB:function(){
            return $http.get("/appDetails");
        },
        getAppMapperConfig:function(){
            return $http.get("/appSensorMappingConfig");
        },
        setAppMapperConfigConstants:function (appConfigMapper) {
            appConfigMapperConstants = appConfigMapper;
        },
        getAppMapperConfigConstants: function () {
            return appConfigMapperConstants;
        },
        getMapConfigDetails:function(){
            return $http.get("/configMapDetails");
        },
        getAppDocumentationDetailById:function(appId){
            var pulseApplicationUrl=appService.getPulseApiConstants();
            return $http.get(pulseAppBaseUrl+pulseApplicationUrl.appApiUrl+'/'+appId+'/documentation');
        }

    }
    return appService;


}]);
