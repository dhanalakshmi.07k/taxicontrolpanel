/**
 * Created by zendynamix on 8/6/2016.
 */
taxiFleetControlPanel.service('constantService',['$http', function($http){
        var assetConfig={};
        var gpsWoxConstant={};
        var getConstant = function () {
                return $http.get("/constants");
        }
        var  getAssetsConfig = function(){
                return $http.get('/assetConfig');
        }

        var setAssetConfig=function(assetConfigData){
                assetConfig=assetConfigData;
        }
        var getAssetConfig=function(){
                return assetConfig;
        }
        var getGpsDeviceWoxConstantFromApi=function(){
                return $http.get('/gpsDeviceWoxConstant');

        }
        var setGpsDeviceWoxConstant=function(constants){
                gpsWoxConstant=constants;
        }
        var getGpsDeviceWoxConstant=function(){
                return gpsWoxConstant;
        }
        return{
                getConstant:getConstant,
                getAssetsConfig:getAssetsConfig,
                setAssetConfig:setAssetConfig,
                getAssetConfig:getAssetConfig,
                getGpsDeviceWoxConstantFromApi:getGpsDeviceWoxConstantFromApi,
                setGpsDeviceWoxConstant:setGpsDeviceWoxConstant,
                getGpsDeviceWoxConstant:getGpsDeviceWoxConstant
        }
}])