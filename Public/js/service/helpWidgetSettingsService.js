/**
 * Created by Suhas on 6/20/2016.
 */
taxiFleetControlPanel.service('helpWidgetService',['$http', function($http) {
    var pageHelpDescription = '';
    var pageCodeForHelpDescription = '';
    var getHelpWidgets = function(){
        return $http.get('/helpWidgets');
    }
    var addHelpWidget = function(obj){
        return $http.post('/helpWidgets',obj);
    }
    var updateHelpWidget = function(obj){
        return $http.put('/helpWidgets',obj);
    }
    var deleteHelpWidget = function(id){
        return $http.delete('/helpWidgets/'+id);
    }
    var getHelpWidgetContentByPageCode = function(pageCode){
        if(!pageCode){
            pageCode='App'
        }
        return $http.get('/helpWidgets/content/'+pageCode);
    }
    var setPageCodeForHelpDescription = function(pageCode){
        pageCodeForHelpDescription = pageCode;
    }
    var getPageCodeForHelpDescription = function(){
        return pageCodeForHelpDescription;
    }
    var isPageCodeContentAlreadyExist=function(pageCode){
        return $http.get('/helpWidgets/isContentAlreadyExist/'+pageCode);

    }
    return{
        getHelpWidgets:getHelpWidgets,
        addHelpWidget:addHelpWidget,
        updateHelpWidget:updateHelpWidget,
        deleteHelpWidget:deleteHelpWidget,
        getHelpWidgetContentByPageCode:getHelpWidgetContentByPageCode,
        setPageCodeForHelpDescription:setPageCodeForHelpDescription,
        getPageCodeForHelpDescription:getPageCodeForHelpDescription,
        isPageCodeContentAlreadyExist:isPageCodeContentAlreadyExist
    }
}])