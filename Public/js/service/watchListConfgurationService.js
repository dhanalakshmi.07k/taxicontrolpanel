/**
 * Created by zendynamix on 15-06-2016.
 */

taxiFleetControlPanel.factory('watchListConfigurationService',function($http){
    var getPersonWatchListConfiguration= function(){
        return $http.get('/apps/watchlist/persons/config')
    }
    return{
        getPersonWatchListConfiguration:getPersonWatchListConfiguration
    }
})
