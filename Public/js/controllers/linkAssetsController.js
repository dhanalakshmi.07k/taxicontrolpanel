/**
 * Created by Suhas on 8/11/2016.
 */
taxiFleetControlPanel.controller('linkAssetsCtrl', function ($scope,constantService,
                                                             sensorService,linkAssetsService) {
        $scope.value={
                assets:{}
        }/*$scope.linkAssetsVariables.mainTab.selected*/
        $scope.assetDataToBeLinked={};
        $scope.linkAssetsVariables = {
                mainTab:{
                        selected:'car',
                        assetLabel:'Car'
                },
                car:{
                        previous:{isExist:false},
                        next:{isExist:true,typeId:'obdDevice',label:'OBD Device'},
                        data:''
                },
                obdDevice:{
                        next:{isExist:true,typeId:'rfIdTag',label:'RFID Tag'},
                        previous:{isExist:true,typeId:'car',label:'Car'},
                        data:''
                },
                rfIdTag:{
                        next:{isExist:true,typeId:'driver',label:'Driver',data:''},
                        previous:{isExist:true,typeId:'obdDevice',label:'OBD Device'},
                        data:''
                },
                driver:{
                        next:{isExist:false,typeId:'driver',label:'Driver',data:''},
                        previous:{isExist:true,typeId:'rfIdTag',label:'RFID Tag'},
                        data:''
                },
                subTab:{
                        labels:['New Car','Existing Car']
                },
                carId:0,
                assetsFormDetails:[],
                assetLinkedDetails:[],
                assets:{},
                constants:'',
                linkedAssetsLists:[],
                paginationDetails:{
                        maxDocsPerPage:10,
                        maxNoOfPages:10,
                        selectedPage:1,
                        totalNoOfPages:0,
                        noOfPagesToBeDisplaced:10
                },
                pointerDirection:'next',
                slider:{
                        previous:slideTheLink,
                        data:''
                }
        }
        $scope.mainTabSelected = function(assetType,assetLabel,id){
               /* if($scope.iconClicked && $scope.iconClicked!=''){
                        $scope.linkAssetsVariables.mainTab.selected = assetType;
                        $scope.assetType=assetType;
                        $scope.linkAssetsVariables.mainTab.assetLabel=assetLabel;
                        getAssetTypeConfiguration();
                }else{*/
                        $scope.linkAssetsVariables.mainTab.selected = assetType;
                        $scope.assetType=assetType;
                        $scope.linkAssetsVariables.mainTab.assetLabel=assetLabel;
                        if($scope.linkAssetsVariables[assetType].data!=''){
                                $scope.formEdit=true;
                                getFormDetailsByAssetType();
                        }
               /* }*/
                if(id){
                        getAssetTypeConfiguration();
                        sensorService.getSensorDetailsById(id).then(function(response){
                                if(response && response.data){
                                        $scope.sensordetails = response.data;
                                        getSensorConfiguration($scope.sensordetails.sensorData.sensorType)
                                        $scope.pageLoader=false;
                                        if($scope.sensordetails.sensorData.latitude
                                        && $scope.sensordetails.sensorData.latitude){
                                                var lat = parseFloat($scope.sensordetails.sensorData.latitude);
                                                var lng = parseFloat($scope.sensordetails.sensorData.longitude);
                                                var type = $scope.sensordetails.sensorData.sensorType;
                                                var sensorName = $scope.sensordetails.sensorData.sensorName;

                                                initMap(lat,lng,type,sensorName);
                                        }
                                        sensorService.setSensorNameOfSensorInSensorDetails(sensorName);
                                }
                        })
                }

        }

        $scope.saveAssetDetails = function(){
                $scope.value.assets.sensorType=$scope.linkAssetsVariables.mainTab.selected;
                $scope.value.assets.sensorLabel=$scope.linkAssetsVariables.mainTab.assetLabel;
                if($scope.linkAssetsVariables[$scope.value.assets.sensorType].data===''){
                        var assetNameVariable= new Object($scope.value.assets.assetName);
                        $scope.value.assets.assetName=$scope.value.assets[assetNameVariable];
                }
                $scope.value.assets.islinked = true;
                if($scope.value.assets.sensorType==='car'){
                        var carData = $scope.linkAssetsVariables['car'].data;
                        if(carData!=''){
                                sensorService.getSensorDetailsById(carData._id).then(function(result){
                                        var obj = {
                                                _id:result.data._id
                                        }
                                        if(result.data && result.data.sensorData && result.data.sensorData.assetsLinked){
                                                $scope.value.assets.assetsLinked= result.data.sensorData.assetsLinked;
                                                obj.sensorData = $scope.value.assets
                                                updateAssetDataAndNavigate(obj)
                                        }else{
                                                obj.sensorData = $scope.value.assets
                                                updateAssetDataAndNavigate(obj)
                                        }
                                })
                        }else{
                                /*updateAssetDataAndNavigate(obj)*/
                                saveAssetData()
                        }
                }else{
                        var assetData = $scope.linkAssetsVariables[$scope.value.assets.sensorType].data;
                        if(assetData!=''){
                                var obj = {
                                        _id:assetData._id,
                                        sensorData:$scope.value.assets
                                }
                                updateAssetDataAndNavigate(obj)
                        }else{
                                saveAssetData()
                        }

                }
        }

        function updateAssetDataAndNavigate(obj){
                sensorService.updateSensorDetails(obj).then(function(result){
                        $scope.linkAssetsVariables[$scope.linkAssetsVariables.mainTab.selected].data = result.data;
                        $scope.value.assets='';
                        $scope.moveToNextAsset();
                })
        }
        function saveAssetData(){
                sensorService.addSensors($scope.value.assets).then(function(result){
                        $scope.linkAssetsVariables[$scope.linkAssetsVariables.mainTab.selected].data = result.data;
                        $scope.value.assets='';
                   /*     if(!$scope.linkingEnded){*/
                                if($scope.linkAssetsVariables.mainTab.selected==='car'){
                                        $scope.linkAssetsVariables.carId=result.data._id;
                                        slideToNextAssetToLink();
                                }
                                else if($scope.linkAssetsVariables.mainTab.selected!='car'
                                        && $scope.linkAssetsVariables.carId!=0){
                                        var obj = {
                                                type:result.data.sensorData.sensorType,
                                                name:result.data.sensorData.assetName,
                                                id:result.data._id
                                        }
                                        linkAssets($scope.linkAssetsVariables.carId,obj)
                                }
                        /*}*/
                })
        }
        function linkAssets(assetLinkedTo,assetToBeLinked){
                var linkAssetsDetails = {
                        assetToBeLinked:assetToBeLinked,
                        assetLinkedTo:assetLinkedTo
                }
                linkAssetsService.linkAssets(linkAssetsDetails).then(function(response){
                        $scope.moveToNextAsset();
                })
        }
        function slideToNextAssetToLink(){
                var currentTab = new Object($scope.linkAssetsVariables.mainTab.selected);
                var direction = $scope.linkAssetsVariables.pointerDirection;
                /*if(currentTab!='driver'){*/
                $scope.linkAssetsVariables.mainTab.selected = $scope.linkAssetsVariables[currentTab][direction].typeId;
                $scope.linkAssetsVariables.mainTab.assetLabel = $scope.linkAssetsVariables[currentTab][direction].label;
                /*getAssetTypeConfiguration();*/
                getFormDetailsByAssetType()
                /*}else{
                 getLinkedAssetCount();
                 $state.go('necApp.linksMain.assetLinks')
                 }*/
        }

        function getConstants(){
                constantService.getConstant().then(function(result){
                        $scope.linkAssetsVariables.constants = result.data.constants.ASSET_DETAILS;
                })
        }
        function getAssetTypeConfiguration(){
                sensorService.getAssetTypeConfigByAssetType($scope.linkAssetsVariables.mainTab.selected)
                    .then(function(result){
                            getAssetFormDetails(result.data.sensorConfiguration.configuration);
                    })
        }
        function getAssetFormDetails(assetTypeConfiguration){
                sensorService.generateHtmlFromJson1(assetTypeConfiguration,function(data){
                        $scope.form=data;
                        $scope.linkAssetsVariables.assetsFormDetails=data;

                        /* $scope.newLinkAssetForm.$setPristine();*/
                })
        }
        function getLinkedAssetList(){
                var maxAssetPerPage = $scope.linkAssetsVariables.paginationDetails.maxDocsPerPage;
                var selectedPageNo = $scope.linkAssetsVariables.paginationDetails.selectedPage;
                var skip =((selectedPageNo-1)*maxAssetPerPage)
                var limit=maxAssetPerPage;
                linkAssetsService.getLinkedAssetsLists(skip,limit,'car').then(function(result){
                        $scope.linkAssetsVariables.linkedAssetsLists = result.data;
                })
        }
        function getLinkedAssetCount(){
                linkAssetsService.getLinkedAssetsCount('car').then(function(result){
                        var totalPageNoToBeDisplayed = parseInt(result.data.count)/$scope.linkAssetsVariables.paginationDetails.maxDocsPerPage;
                        var roundedTotalPageNo = Math.round(parseInt(result.data.count)/$scope.linkAssetsVariables.paginationDetails.maxDocsPerPage);
                        if(totalPageNoToBeDisplayed>roundedTotalPageNo){
                                totalPageNoToBeDisplayed =roundedTotalPageNo+1;
                        }else if(totalPageNoToBeDisplayed < 1){
                                totalPageNoToBeDisplayed=1;
                        }else if(totalPageNoToBeDisplayed<roundedTotalPageNo){
                                totalPageNoToBeDisplayed=roundedTotalPageNo;
                        }
                        $scope.linkAssetsVariables.paginationDetails.totalNoOfPages=totalPageNoToBeDisplayed;
                        if(totalPageNoToBeDisplayed<=$scope.linkAssetsVariables.paginationDetails.maxPageNo){
                                $scope.linkAssetsVariables.paginationDetails.noOfPagesToBeDisplaced = range(totalPageNoToBeDisplayed);
                        }else{
                                $scope.linkAssetsVariables.paginationDetails.noOfPagesToBeDisplaced = range($scope.linkAssetsVariables.paginationDetails.maxPageNo);
                        }
                        getLinkedAssetList();
                })
        }

        function range(n) {
                return new Array(n);
        };
        $scope.setDirectionOfPointer = function(direction){
                $scope.linkAssetsVariables.pointerDirection=direction;
        }
        $scope.getAssetsLinkedByPageNo = function(pageNo){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=pageNo;
                getLinkedAssetList();
        }
        $scope.increment = function(){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=$scope.linkAssetsVariables.paginationDetails.selectedPage+1;
                getLinkedAssetList();
        }
        $scope.decrement = function(){
                $scope.linkAssetsVariables.paginationDetails.selectedPage=$scope.linkAssetsVariables.paginationDetails.selectedPage-1;
                getLinkedAssetList();
        }
        function linkAssetsCtrlInit(){
                getConstants();
                getAssetTypeConfiguration();
                /*getLinkedAssetList();*/
                getLinkedAssetCount();
        }
        $scope.range = function(n) {
                return new Array(n);
        };
        linkAssetsCtrlInit();


        $scope.formEdit = false;
        $scope.moveToNextAsset = function(){
                var direction = $scope.linkAssetsVariables.pointerDirection;
                var tabSelected = $scope.linkAssetsVariables.mainTab.selected;
                var typeIdToBeSlided = $scope.linkAssetsVariables[tabSelected][direction].typeId;
                var data = $scope.linkAssetsVariables[typeIdToBeSlided].data;
                if(data!=''){
                        $scope.formEdit = true;
                        $scope.linkAssetsVariables.slider.data = data.sensorData;
                        slideTheLink();
                }else{
                        $scope.formEdit = false;
                        $scope.linkAssetsVariables.slider.data = '';
                        slideTheLink();
                }
        }
        function slideTheLink(){
                var currentTab = new Object($scope.linkAssetsVariables.mainTab.selected)
                $scope.linkAssetsVariables.mainTab.selected = $scope.linkAssetsVariables[currentTab][$scope.linkAssetsVariables.pointerDirection].typeId;
                $scope.linkAssetsVariables.mainTab.assetLabel = $scope.linkAssetsVariables[currentTab][$scope.linkAssetsVariables.pointerDirection].label;
                getFormDetailsByAssetType();
        }
        function getFormDetailsByAssetType(){
                sensorService.getFormDetailsByAssetType
                ($scope.linkAssetsVariables.mainTab.selected).then(function(data){
                        $scope.value.assets='';
                        $scope.linkAssetsVariables.assetsFormDetails=data;
                        if($scope.linkAssetsVariables[$scope.linkAssetsVariables.mainTab.selected].data!=''){
                                $scope.formEdit=true;
                                assignValueForForm()
                        }else{
                                $scope.formEdit=false;
                        }
                })
        }
        function assignValueForForm(){
                var tabSelected = $scope.linkAssetsVariables.mainTab.selected;
                var data = $scope.linkAssetsVariables[tabSelected].data.sensorData;
                $scope.value.assets = data;
        }
        function getSensorConfiguration(sensorType){
                sensorService.getFormDetailsByAssetType(sensorType,function(result){
                        if(result){
                                $scope.form=result;
                        }

                })
        }
        $scope.clearData = function(){
                linkAssetsCtrlInit()
        }


        /*navigation on selection tabs*/

        $scope.changeTab = function(assetType,assetLabel,saveFlag){
                if(saveFlag){
                        $scope.previousTypeId = angular.copy($scope.linkAssetsVariables.mainTab.selected);
                        $scope.previousLabel = angular.copy($scope.linkAssetsVariables.mainTab.assetLabel);
                        $scope.linkAssetsVariables.mainTab.selected = assetType;
                        $scope.linkAssetsVariables.mainTab.assetLabel=assetLabel;
                        checkDataAlreadyPresent($scope.previousTypeId,$scope.previousLabel)
                }else{
                        $scope.linkAssetsVariables.mainTab.selected = assetType;
                        $scope.linkAssetsVariables.mainTab.assetLabel=assetLabel;
                      /*  if($scope.linkAssetsVariables[assetType].data!=''){
                                $scope.formEdit=true;
                        }else{
                                $scope.formEdit=false;
                        }*/
                        getFormDetailsByAssetType()
                }

        }
        function checkDataAlreadyPresent(typeId,label){
                $scope.value.assets.sensorType=typeId;
                $scope.value.assets.sensorLabel=label;

                if($scope.linkAssetsVariables[typeId].data===''){
                        var assetNameVariable= new Object($scope.value.assets.assetName);
                        $scope.value.assets.assetName=$scope.value.assets[assetNameVariable];
                }
                $scope.value.assets.islinked = true;
                if($scope.value.assets.sensorType==='car'){
                        var carData = $scope.linkAssetsVariables['car'].data;
                        if(carData!=''){
                                sensorService.getSensorDetailsById(carData._id).then(function(result){
                                        var obj = {_id:result.data._id}
                                        if(result.data && result.data.sensorData && result.data.sensorData.assetsLinked){
                                                $scope.value.assets.assetsLinked= result.data.sensorData.assetsLinked;
                                                obj.sensorData=$scope.value.assets;
                                                updateAssetDetails(obj);
                                        }else{
                                                obj.sensorData=$scope.value.assets;
                                                updateAssetDetails(obj);
                                        }
                                })
                        }
                        else{
                                saveAssetDataByTabClick(typeId)
                        }
                }
                else{
                        var assetData = $scope.linkAssetsVariables[typeId].data;
                        if(assetData!=''){
                                var obj = {
                                        _id:assetData._id,
                                        sensorData:$scope.value.assets
                                }
                                updateAssetDetails(obj)
                        }
                        else{
                                saveAssetDataByTabClick(typeId)
                        }
                }

        }
        $scope.linkingEnded = false;
        $scope.newLinkAssetDefault = function(){
                $scope.linkingEnded = false;
                $scope.formEdit=false;
                $scope.linkAssetsVariables.car.data = '';
                $scope.linkAssetsVariables.obdDevice.data = '';
                $scope.linkAssetsVariables.rfIdTag.data = '';
                $scope.linkAssetsVariables.driver.data = '';
                $scope.linkAssetsVariables.mainTab.selected='car'
                $scope.linkAssetsVariables.mainTab.assetLabel='Car'
                $scope.linkAssetsVariables.subTab.labels=['New Car','Existing Car'];
                $scope.tabSelectedConfirmed = true;
                $scope.tabSelectedPopUp = false;
                getConstants();
                getAssetTypeConfiguration();
        }
        function saveAssetDataByTabClick(type){
                sensorService.addSensors($scope.value.assets).then(function(result){
                        $scope.linkAssetsVariables[type].data = result.data;
                        $scope.value.assets='';
                        if(type==='car'){
                                $scope.linkAssetsVariables.carId=result.data._id;
                        }
                        else if(type!='car' && $scope.linkAssetsVariables.carId!=0){
                                var obj = {
                                        type:result.data.sensorData.sensorType,
                                        name:result.data.sensorData.assetName,
                                        id:result.data._id
                                }
                                var linkAssetsDetails = {
                                        assetToBeLinked:obj,
                                        assetLinkedTo:$scope.linkAssetsVariables.carId
                                }
                                linkAssetsService.linkAssets(linkAssetsDetails).then(function(response){
                                        /*$scope.moveToNextAsset();*/
                                })
                        }
                        getFormDetailsByAssetType()

                })
        }
        function updateAssetDetails(assetObj){
                sensorService.updateSensorDetails(assetObj).then(function(result){
                        $scope.linkAssetsVariables[result.data.sensorData.sensorType].data=  result.data.sensorData;
                        getFormDetailsByAssetType()
                })
        }


        $scope.tabSelectedOnDirty = function(type,label){
                $scope.typeSelectedOnDIrty = type;
                $scope.labelSelectedOnDIrty = label;
                $scope.tabSelectedConfirmed = false;
                $scope.tabSelectedPopUp = true;

        }


        $scope.selectAssetsTab = function (assetType, assetLabel, id, carId) {
                addVariableToLink (assetType, assetLabel, carId);
                $scope.linkAssetsVariables.mainTab.selected = assetType;
                $scope.assetType = assetType;
                $scope.carId=carId;
                $scope.linkAssetsVariables.mainTab.assetLabel = assetLabel;
                /*if ($scope.linkAssetsVariables[assetType].data != '') {
                        $scope.formEdit = true;
                        getFormDetailsByAssetType();
                }*/
                $scope.sensordetails='';
                getAssetTypeConfiguration();
                sensorService.getSensorDetailsById(id).then(function (response) {
                        if (response && response.data) {
                                $scope.sensordetails = response.data;
                                getSensorConfiguration($scope.sensordetails.sensorData.sensorType)
                                //$scope.pageLoader = false;
                                //sensorService.setSensorNameOfSensorInSensorDetails(sensorName);
                        }
                })
        }

        function addVariableToLink (assetType, assetLabel, carId) {
                $scope.assetTypeLink=assetType;
                $scope.assetLabelLink=assetLabel;
                $scope.carIdLink=carId;
        }

        $scope.linkAssetDetails = function(assetType,assetLabel){
                $scope.addSensorLoader=true;
                $scope.assetDataToBeLinked.sensorType=assetType;
                $scope.assetDataToBeLinked.sensorLabel=assetLabel;
                var assetNameVariable= new Object($scope.assetDataToBeLinked.assetName);
                $scope.assetDataToBeLinked.assetName=$scope.assetDataToBeLinked[assetNameVariable];

                $scope.assetDataToBeLinked.islinked = true;
                sensorService.addSensors($scope.assetDataToBeLinked).then(function(result){
                        var obj = {
                                type:result.data.sensorData.sensorType,
                                name:result.data.sensorData.assetName,
                                id:result.data._id
                        }
                        var linkAssetsDetails = {
                                assetToBeLinked:obj,
                                assetLinkedTo:$scope.carIdLink
                        }
                        linkAssetsService.linkAssets(linkAssetsDetails).then(function(response){
                                $scope.addSensorLoader=false;
                                getLinkedAssetCount()
                                $scope.closePopup('addSensor');

                        })
                })

        }

        $scope.deLinkAsset = function(assetType,assetId,carId){
                $scope.delinkAssetData = {
                        assetToBeDeLinkedObj:{
                                type:assetType,
                                id:assetId
                        },
                        assetLinkedTo:carId
                }

        }
        $scope.deleteLink = function () {
                linkAssetsService.deLinkAssets($scope.delinkAssetData).then(function(data){
                        getLinkedAssetCount()
                })
        }
        /*$scope.$watch('assetDataToBeLinked.car',function(){
                var oldData1 = $scope.assetDataToBeLinked.car.data;
        })*/

})