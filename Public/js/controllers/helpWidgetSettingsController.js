/**
 * Created by Suhas on 6/20/2016.
 */

taxiFleetControlPanel.controller("helpWidgetController", function ($scope,helpWidgetService,$sce) {
    $scope.helpWidget={/*$scope.helpWidget.addDetails.isSelectedPageCodeContentAlreadyPresent*/
        addDetails:{
            selectedPageCode:"Overview",
            isSelectedPageCodeContentAlreadyPresent:false
        },
        widgetDetails:'',
        idForDelete:'',
        type:"add",
        pageCode:'Settings'
    }
    $scope.addDetails = {
        pageCode:"Overview"
    }
    function getHelpWidgets(){
        helpWidgetService.getHelpWidgets().then(function(result){
            console.log(result)
            if(result && result.data){
                $scope.helpWidget.widgetDetails = result.data
            }
        })
    }
    $scope.addHelpWidget=function(){
        $scope.addDetails.pageCode=$scope.helpWidget.addDetails.selectedPageCode;
        helpWidgetService.addHelpWidget($scope.addDetails).then(function(result){
            $scope.closePopup('addNewHelpWidget');
            // $(".addNewHelpWidget").fadeOut(250, function () {
            //     $(".background").fadeOut(250);
            // });
            if(result){
                $scope.addDetails='';
                $scope.addDetails={
                    pageCode:"Overview"
                };
                console.log("Data Saved");
                getHelpWidgets();
            }
        })

    }
    $scope.setPageCode = function(pageCode){
        $scope.helpWidget.addDetails.selectedPageCode=pageCode;
        isPageContentAlreadyExist(pageCode)
    }
    $scope.setIdForDelete = function(id){
        $scope.helpWidget.idForDelete = id;
    }
    $scope.deleteSelectedHelpWidget = function(){
        helpWidgetService.deleteHelpWidget($scope.helpWidget.idForDelete).then(function(result){
            $scope.closePopup('deleteHelpWidget');
            // $(".deleteHelpWidget").fadeOut(250, function () {
            //     $(".background").fadeOut(250);
            // });
            console.log("deleted successfully");
            getHelpWidgets();
        })
    }
    $scope.editDataForHelpWidget = function(data){
        $scope.addDetails.pageCode=angular.copy(data.pageCode);
        $scope.helpWidget.type = 'edit';
        $scope.addDetails = angular.copy(data);
    }
    $scope.updateDataForHelpWidget = function(data){
        helpWidgetService.updateHelpWidget($scope.addDetails).then(function(result){
            $scope.closePopup('addNewHelpWidget');
            // $(".addNewHelpWidget").fadeOut(250, function () {
            //     $(".background").fadeOut(250);
            // });
            if(result){
                $scope.addDetails = '';
                $scope.helpWidget.type = 'add';
                $scope.addDetails = {
                    pageCode:"Overview"
            }
                console.log("Updated")
                getHelpWidgets();
            }
        })
    }
    function getPageHelpDescription(){
        var pageDescription = helpWidgetService.getHelpWidgetContentByPageCode();
        helpWidgetService.getHelpWidgetContentByPageCode(pageDescription).then(function(result){
            if(result && result.data){
                $scope.helpContent = $sce.trustAsHtml(result.data.widgetDescription)
            }
        })
    }
    function setHelpWidgetContentPageCode(){
        helpWidgetService.setPageCodeForHelpDescription($scope.helpWidget.pageCode);
    }
    function isPageContentAlreadyExist(pageCode){
        helpWidgetService.isPageCodeContentAlreadyExist(pageCode).then(function(result){
            $scope.helpWidget.addDetails.isSelectedPageCodeContentAlreadyPresent=result.data;
            console.log(result.data)
        })
    }
    function init(){
        getHelpWidgets();
        setHelpWidgetContentPageCode();
        getPageHelpDescription();
        isPageContentAlreadyExist($scope.helpWidget.addDetails.selectedPageCode)
    }
    init();
})
