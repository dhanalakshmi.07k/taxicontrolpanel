/**
 * Created by zendynamix on 7/15/2016.
 */
taxiFleetControlPanel.controller("serverDetailsController", function ($scope,nodeService,$stateParams,appService) {
    $scope.serverDetails={
        showServerDetailsLoader:false
    }

    if ($stateParams.severId) {
        console.log($stateParams.severId)
        $scope.serverDetails.showServerDetailsLoader = true;
        getServerDetailsByName($stateParams.severId);
    }
    $scope.getApplicationConfig = function () {
        $scope.serverDetails.appConstant = appService.getApplicationConstants();
    }
    $scope.getApplicationConfig();

    function getServerDetailsByName(serverId){
        var nodeDetails;
        nodeService.getAllNodes().then(function(response){
            for(var b=0;b<response.data.length;b++){
                if(serverId===response.data[b].host){
                    nodeDetails=response.data[b];
                }
            }
            $scope.serverDetailsByName=nodeDetails;
            $scope.serverDetails.showServerDetailsLoader=false;
        });
    }

})
