/*
 * Created by MohammedSaleem on 29/03/16.
 */

taxiFleetControlPanel.controller("overviewController", function ($scope,$location,sensorService,
             appService,nodeService,settingService,$timeout,$rootScope,instanceService,assetApiService) {

    var map;
    $scope.overviewRelated ={
        servers:{
            count:'-',
            up:'-',
            down:'-'
        },
        instance:{
            count:'-',
            up:'-',
            down:'-'
        },
        app:{
            count:'-'
        },
        sensors:{
            count:'-'
        },
        deviceInfo:{
            data:[],
            selectedDeviceType:'',
            selectedDeviceName:''

        },
        deviceInfoNew:{},
        constants:[],
        appConstant:""
    }
    //   default center of google maps coordinates
    $scope.getMapCoordinates=function(){
             settingService.getMapConfig().then(function(response){
                 if(response && response.data){
                     var mapDetailsObj =response.data[0];
                     var map={
                         centerLatitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude,
                         centerLongitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude
                     };
                     settingService.setMapConfigObj(map);
                     initMap();
                 }
             })
         };
    function getAssetsCount(){
        sensorService.getSensorCount().then(function(res){
            $scope.overviewRelated.sensors.count=res.data.count;
        })
    }
    function getAssetsDetails(){
        assetApiService.getDevicesDetails().then(function (result) {
            if(result){
                $scope.overviewRelated.deviceInfo.data = result;
            }

        }, function error(errResponse) {
            console.log(errResponse);
        });

    }
    function getAllDeviceFromApi() {
        assetApiService.getMapPlotsFromApi().then(function (result) {
            if(result){
                plotMarkersFromApi(result)
            }


        }, function error(errResponse) {
            console.log(errResponse);
        });
    }
    function plotMarkersFromApi(deviceDetailsArray){
        for(var k=0;k<deviceDetailsArray.length;k++){
            if(deviceDetailsArray[k].lat!=0&&deviceDetailsArray[k].lng!=0){
                $scope.addMarkers('camera',deviceDetailsArray[k].lat,deviceDetailsArray[k].lng,deviceDetailsArray[k].id,false)
            }

        }
    }


    //map
    function initMap() {
        var mapBackground='#1d212f';
        if ($("body").hasClass("goldPlatedTheme")){
            mapBackground="#ffffff";
        }
        // Create a new StyledMapType object
        var styledMap = new google.maps.StyledMapType(
            $scope.mapStyles,
            {name: "Styled Map"});
        var mapCenterConfigObj=settingService.getMapConfigObj();
        var mapLatLng = {lat:mapCenterConfigObj.centerLatitude, lng: mapCenterConfigObj.centerLongitude};
        var mapOptions = {
            center: mapLatLng,
            scrollwheel: false,
            zoom: 12,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            backgroundColor: mapBackground,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };

        map = new google.maps.Map(document.getElementById('overviewMap'), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        if (!$("body").hasClass("goldPlatedTheme")){
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }
    }
    $rootScope.$on('mapResize',function () {
        if(!$scope.showNotification){
            $timeout(function () {
                google.maps.event.trigger(map, "resize");
            },1000)
        }
    });
    $scope.getApplicationConfig = function () {
        $scope.overviewRelated.appConstant = appService.getApplicationConstants();
    }
    var showDeviceData= function (element) {
        $(".deviceIndicatorMain").removeClass("active");

        $(element).parent().addClass("active");

        var dataBox= $(".mapContainer .rightDetails");

        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });

        var deviceId=parseInt($(element).attr("id"));
        var devices = $scope.overviewRelated.deviceInfo.data;
        for(i=0;i<devices.length;i++){
            var deviceObj = devices[i];
            if(deviceObj.id===deviceId){
                $scope.overviewRelated.deviceInfoNew.lat=deviceObj.lat
                $scope.overviewRelated.deviceInfoNew.lng = deviceObj.lng;
                $scope.overviewRelated.deviceInfoNew.name = deviceObj.name;
                $scope.overviewRelated.deviceInfoNew.online = deviceObj.online;
                $scope.overviewRelated.deviceInfoNew.updatedDate = new Date(deviceObj.updatedDate);
                $scope.overviewRelated.deviceInfoNew.createdDate = new Date(deviceObj.createdDate);
                $scope.$apply();
                break;
            }

        }
    };
    $("body").on("click",".deviceIndicator",function (e) {
        e.stopPropagation();
        showDeviceData(this);
    });
    $scope.addMarkers = function (type, lat, lng, id ,mapPanDisableFlag) {
        var boxText = '<div class="deviceIndicatorMain"><div class="deviceIndicator ' + type + '" id="' + id + '"></div></div>';
        var myOptions = {
            content: boxText,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-15, -15),
            zIndex: null,
            enableEventPropagation: true,
            closeBoxURL: "",
            position: new google.maps.LatLng(lat, lng),
            disableAutoPan:mapPanDisableFlag
        };

        var ib = new InfoBox(myOptions);
        ib.open(map,$scope.mapOptions);
    };
    



function init(){
    $scope.getMapCoordinates();
    getAssetsDetails();
    getAssetsCount();
    getAllDeviceFromApi();
}
init();


























});



