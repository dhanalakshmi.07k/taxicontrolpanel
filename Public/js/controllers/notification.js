/**
 * Created by Suhas on 4/2/2016.
 */
taxiFleetControlPanel.controller("notificationController", function ($scope,$rootScope,notificationService) {
        $scope.notificationDetails={
                notificationArray:[],
                notificationArrayLength:0
        }
        $rootScope.$on(notificationService.notificationConstants.NOTIFICATION_DATA_NOTIFIER,function(data){
                notifyNotification()
                $scope.$apply();
        })
        function notifyNotification(){
                $scope.notificationDetails.notificationArrayLength=$scope.notificationDetails.notificationArray.length;
                if($scope.notificationDetails.notificationArrayLength>25){
                        $scope.notificationDetails.notificationArray.pop();
                        $scope.notificationDetails.notificationArray.unshift(notificationService.getNotification())
                }else{
                        $scope.notificationDetails.notificationArray.unshift(notificationService.getNotification())
                }
        }

})