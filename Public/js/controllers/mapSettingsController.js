/**
 * Created by zendynamix on 7/8/2016.
 */
taxiFleetControlPanel.controller('settingsController', function ($scope,$http,$state,settingService,appService) {
$scope.mapSettingsdetails={

}
    $scope.saveMapConfiguration=function(mapSettingsDetails){
        console.log(mapSettingsDetails);
        settingService.addMapConfigDetails(mapSettingsDetails).then(function(err,res){
            settingService.getMapConfig().then(function(err,result){
               console.log(result)
            })

        })
    }
    $scope.getMapConfigData=function(){
        settingService.getMapConfig().then(function(response){
            if(response && response.data){
                var mapDetailsObj =response.data[0];
                $scope.map={
                    centerLatitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude,
                    centerLongitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude
                }
            }
        })
    }
    function init(){
        $scope.getMapConfigData();
    }
    init();

})