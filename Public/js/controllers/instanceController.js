/**
 * Created by zendynamix on 7/9/2016.
 */

taxiFleetControlPanel.controller("instancesController", function ($scope, $location, watchListService, appService, instanceService, $stateParams, $state, $timeout, $http, jsonFormService, nodeService) {
    $scope.instanceObjDetails = {}
    $scope.instanceDetails = {
        instanceState: false,
        instanceLoaderState: false,
        showSaveInstanceLoader: false,
        showCloneInstanceLoader: false,
        instanceLoaderStatus: false,
        loadInstanceByIdStatus: false,
        showDeleteInsatnce: false,
        instanceConfigurationLoader: false,
        appConstant: "",
        showDeleteFileLoader:false,
        saveInstanceFile:false,
        instanceCount:0,
        instanceLoaderStatusById: false,
        appInstanceId:""


    };
    $scope.getApplicationConfig = function () {
        $scope.instanceDetails.appConstant = appService.getApplicationConstants();
    }
    $scope.getAllInstanceDetails=function() {
        $scope.instanceDetails.instanceLoaderState = true;
        instanceService.getInstanceDetails().then(function (result) {
            $scope.resultantinstanceDetails = result.data;
            $scope.instanceDetails.instanceLoaderState = false;

        });
    }

    $scope.addInstance = function (instanceObjDetails, instanceObj, tabsType,instanceAppId) {
        $scope.instanceDetails.showSaveInstanceLoader = true;
        var instanceUserName = instanceObjDetails.userInstanceName;
        //var appInstanceSelected = $(".instancesSelected .headTitle").text().trim();
        var appInstanceSelected = instanceAppId;
        var nodeSelected = $(".nodeSelected .headTitle").text().trim();
        var instanceConfigObj = {};
        if (tabsType === "raw") {
            var nodes = {};
            nodes = [nodeSelected];
            instanceConfigObj.appId = appInstanceSelected;
            instanceConfigObj.instanceName = instanceUserName;
            instanceConfigObj.nodes = nodes;
            instanceConfigObj.configuration = JSON.parse(instanceObj);
        }
        else if (tabsType === "form") {
            fromConfigObject($scope.extractedData);
            var nodes = {};
            nodes = [nodeSelected];
            instanceConfigObj.appId = appInstanceSelected;
            instanceConfigObj.instanceName = instanceUserName;
            instanceConfigObj.nodes = nodes;
            instanceConfigObj.configuration = instanceService.getAppConfig();
        }
        instanceService.saveInstance(instanceConfigObj).then(function (result) {
            var instanceFileObjectArray=instanceService.getInstanceFileObjectArray();
            if(instanceFileObjectArray.length){
                $scope.updateInstanceFilesForm(result.data.id)
            }
            else{
                $scope.closeInstance();
            }
        }, function error(errResponse) {
            $scope.instanceDetails.showSaveInstanceLoader = false;
            $scope.appInstallErrorMessage = errResponse.data.message
        })

    }
    function fromConfigObject(formData) {
        var sensorTypeArrayName = appService.getAppMapperConfigConstants();
        var configObj = {};
        var fieldValue;
        var fieldName;
        var fileObjArray = [];

        for (var m = 0; m < formData.length; m++) {
            fieldName = formData[m].realName;
            if (formData[m].model === undefined) {
                fieldValue = null;
            }
            if (sensorTypeArrayName.indexOf(formData[m].type) >= 0) {
                var configValue = jsonFormService.getSensorConfig();
                var dropDownFieldName = formData[m].realName;
                fieldValue = configValue[dropDownFieldName];
            }
            if (formData[m].type === "file" && formData[m].model.name) {
                fileObjArray.push(formData[m].model)
                fieldValue = formData[m].model.name;
            }
            if (formData[m].type === "file" && !formData[m].model.name) {
                fieldValue = formData[m].model;

            }
            if (formData[m].type !== "file" && sensorTypeArrayName.indexOf(formData[m].type) === -1) {
                fieldValue = formData[m].model;
            }
            if (fieldValue !== null) {
                configObj[fieldName] = fieldValue;
            }
        }
        instanceService.setAppConfig(configObj);
        instanceService.setInstanceFileObjectArray(fileObjArray);

    }
    $scope.updateInstanceFilesForm=function(instanceId){
        var instanceFileObjectArray=instanceService.getInstanceFileObjectArray()
        var b=0;
        for( b=0;b<instanceFileObjectArray.length;b++){
            $scope.saveInstanceFileFromForm(instanceId,instanceFileObjectArray[b],instanceFileObjectArray.length)
        }


    }
    $scope.saveInstanceFileFromForm= function (instanceId,fileObject,instanceFileObjectArrayLength) {
        var count=0;
        var file =fileObject;
        //save file changed
        instanceService.saveFileForInstance( file,instanceId).then(function (response) {
            $scope.instanceDetails.instanceCount++;
            if( $scope.instanceDetails.instanceCount===instanceFileObjectArrayLength){
                $scope.closeInstance();
            }
            console.log("added successfully")
        }, function error(errResponse) {
            $scope.errorInSavingFileformForm=errResponse.data.message;
            $scope.instanceDetails.showSaveInstanceLoader = false;
        });

    }

//edit Instance
    if ($stateParams.instanceId) {
        $scope.instanceDetails.loadInstanceByIdStatus = true;
        getInstanceDetailsByIdFromApi($stateParams.instanceId);
    }
    function getInstanceDetailsByIdFromApi(instanceIdFromView) {
        instanceService.getInstanceDetailsFromApi(instanceIdFromView).then(function (responseObj) {
            $scope.instanceObjectDetails = responseObj.data;
            $scope.getInstanceFile(instanceIdFromView);

        })
    }
    $scope.closeInstance=function (instanceId,fileObject) {
    $scope.getAllInstanceDetails()
    $scope.instanceDetails.showSaveInstanceLoader = false;
    $scope.closePopup('addInstance');
}
    $scope.deleteInstanceById = function (instanceId) {
        $scope.instanceDetails.showDeleteInsatnce = true;
        $scope.appDeleteErrorMessage = null;


        instanceService.deleteInstance( instanceId).then(function (result) {
            $scope.getAllInstanceDetails();
                $scope.instanceDetails.showDeleteInsatnce = false;
                $scope.closePopup('deleteInstance');
                $(".messagePopup.instanceDeleted").fadeIn(300);
                $timeout(function () {
                    $state.go('necApp.instanceMain.instance');
                }, 3000);

        }, function error(errResponse) {
            $scope.appDeleteErrorMessage = errResponse.data.message
            $scope.instanceDetails.showDeleteInsatnce = false;
        })
    }
    $scope.getAllAppNames = function () {
        appService.getAppDetails().then(function (result) {
            $scope.appDropDownName = result.data;

        }, function error(errResponse) {
            $scope.dropDownError=result.message.data
        });
    }
    $scope.emptyFormObject = function () {

        $scope.extractedData=[];
        $scope.instanceObjDetails.userInstanceName = "";
        $(".instancesSelected .headTitle").val("");
        $(".nodeSelected .headTitle").val("");
        $scope.tabsType = 'form';
        $scope.getFromBasedOnAppIdDirective = '';
   /*     $scope.getAppConfigurationByAppNameForCreatingAInstance('');
        $scope.getJsonFromDataBasedOnApp('');*/
    }
    $scope.cloneInstance = function (instanceDetails, cloneInstanceName, serverName) {
        $scope.instanceDetails.showCloneInstanceLoader = true;
        instanceService.getInstanceConfigurationById( instanceDetails.id).then(function (result) {
            var cloneConfigObject = {};
            cloneConfigObject.appId = instanceDetails.parameters.appId;
            cloneConfigObject.instanceName = cloneInstanceName;
            cloneConfigObject.nodes = [serverName];
            cloneConfigObject.configuration = result.data;
            $scope.saveCloneInstance(cloneConfigObject);
        }, function error(errResponse) {
            $scope.instanceDetails.showCloneInstanceLoader = false;
            $scope.appCloneInstaneErrorMessage = errResponse.data.message
        })
    }
    $scope.saveCloneInstance=function(cloneConfigObject){
        instanceService.saveInstance(cloneConfigObject).then(function (result) {
            $scope.getAllInstanceDetails();
            $scope.instanceDetails.showCloneInstanceLoader = false;
            $scope.closePopup('cloneInstance');
            $state.go('necApp.instanceMain.instance');
        }, function error(errResponse) {
            $scope.instanceDetails.showCloneInstanceLoader = false;
            $scope.appCloneInstaneErrorMessage = errResponse.data.message
        })
    }
    $scope.getAllServerDetails=function(){
        $scope.instanceUpdateNodeStatusErrorMessage=null;
        var nodesList=[];
        nodeService.getAllNodes().then(function(response){
            for(var k=0;k<response.data.length;k++){
                nodesList.push(response.data[k].host);
            }
            $scope.severList=nodesList;

        });
    }
    $scope.updateNodeByInstanceId=function(instanceId,nodeName){
        $scope.instanceLoaderUpdate = true;
        var nodes = {};
        nodes = [nodeName];
        var nodeObj={}
        nodeObj.nodes=nodes;
        instanceService.updateInstanceNodeDetails( instanceId, nodeObj).then(function (result) {
            getInstanceDetailsByIdFromApi(instanceId);
            $scope.editInstance=false;
            $scope.showMessagePopup('instanceUpdated');
            $scope.instanceLoaderUpdate = false;
        }, function error(errResponse) {
            $scope.instanceLoaderUpdate = false;
            /* $scope.closePopup("instanceManagePopup");*/
            $scope.instanceUpdateNodeStatusErrorMessage = errResponse.data.message
            /* getInstanceDetailsByIdFromApi(instanceId);*/
        })


    }
    $scope.clearInstanceErrorMessageSetInstanceId=function(InstanceId){
        $scope.stopInstanceId=InstanceId;
        $scope.instanceStatusErrorMessage=null;

    }
    $scope.StartInstance = function (instanceId, status, instance) {
        $scope.instanceLoader=true;
        instance.loader = !instance.loader;
        var statusObj = {};
        statusObj.status = status;
        instanceService.updateInstanceStatus( instanceId, statusObj).then(function (result) {

                $scope.getAllInstanceDetails();
                instance.loader = !instance.loader;
                $scope.instanceLoader=false;
                $scope.closePopup("instanceManagePopup");

        }, function error(errResponse) {
            $scope.instanceLoader=false;
            instance.loader = !instance.loader;
            $scope.instanceLoader=false;
            $scope.instanceStatusErrorMessage = errResponse.data.message;
        })
    }
    $scope.StartInstanceById = function (instanceId, status) {
        $scope.instanceDetails.instanceLoaderStatusById = true;
        var statusObj = {};
        statusObj.status = status;
        instanceService.updateInstanceStatus( instanceId, statusObj).then(function (result) {
            getInstanceDetailsByIdFromApi(instanceId);
            $scope.instanceDetails.instanceLoaderStatusById = false;
            $scope.closePopup("instanceManagePopup");

        }, function error(errResponse) {
            $scope.instanceStatusErrorMessage = errResponse.data.message
            $scope.instanceDetails.instanceLoaderStatusById = false;
        })
    }

    //get configuration on selecting the drop down
    $scope.getAppConfigurationByAppNameForCreatingAInstance = function (appId) {
        appService.getAppDetailsByID(appId).then(function (result) {

                $scope.instanceDetails.appInstanceId=appId;
                var configurationJson = result.data.configuration;
                $scope.getFromBasedOnAppIdDirective = appId;
                //call to populate raw data
                getAppConfigurationDetailsById(configurationJson, appId)
                $scope.getJsonFromDataBasedOnApp(appId);


        }, function error(errResponse) {
            $scope.appDetailsError = result.data.message
        });

    }
    $scope.getJsonFromDataBasedOnApp = function (getFromBasedOnAppIdDirective) {
        var fieldValueMapper = {};
        jsonFormService.setSensorConfig(fieldValueMapper);
        $scope.$broadcast('getFromData', {currentAppId: getFromBasedOnAppIdDirective});
    }
    $scope.getJsonFromBasedOnInstanceId = function (instanceId,appId) {
      /*  var fieldValueMapper = {};
        jsonFormService.setSensorConfig(fieldValueMapper);
        var instanceObj={}
        instanceObj.instanceId=instanceId;
        instanceObj.appId=appId;
        console.log("!!!!!!!!!!!!!!!!!!!!!!!!")
        $scope.$broadcast('getFromDataBasedOnInstanceId', {hi:"helo"});*/
    }

    /*get sample*/
    function getAppConfigurationDetailsById(configurationJson, appId) {
        $scope.addInstanceLoader=true;
        appService.getAppConfigurationById(appId).then(function (result) {
            $scope.configData = configurationJson;
            $scope.sampleData = result.data;
            $scope.instanceObj = JSON.stringify(result.data, null, 4);
            $scope.addInstanceLoader=false;
        }, function error(errResponse) {
            $scope.appDetailsError = errResponse.data.message;
            $scope.addInstanceLoader=false;
        });
    }
    $scope.getInstanceConfigDetailsById = function (instanceId) {
        $scope.instanceConfigError = null
        $scope.instanceDetails.instanceConfigurationLoader = true;
        $scope.instanceConfigurationDEtaislByID = "";
        instanceService.getInstanceConfigurationById(instanceId).then(function (result) {
            $scope.instanceIdConfigObj = instanceId
            $scope.instanceConfigurationDetailsByIForModel = JSON.stringify(result.data, null, 4)
            $scope.instanceDetails.instanceConfigurationLoader = false;

        }, function error(errResponse) {
            $scope.configurationerror=errResponse.message.data;
            $scope.instanceDetails.instanceConfigurationLoader = false;
        })

    }
    $scope.UpdateInstanceConfiguration = function (instanceId, instanceConfiguration) {
        $scope.instanceDetails.instanceConfigurationLoader = true;
        var modifiedJson=JSON.stringify(instanceConfiguration, null, 4)
        instanceService.updateInstanceConfigById( instanceId, instanceConfiguration).then(function (result) {
            getInstanceDetailsByIdFromApi(instanceId);
            $scope.instanceDetails.instanceConfigurationLoader = false;
            $(".popup.configDetailsOfInstance").fadeOut(250, function () {
                $(".background").fadeOut(250);
            });

        }, function error(errResponse) {
             $scope.instanceConfigError = errResponse.data
            console.log($scope.instanceConfigError )
            $scope.instanceDetails.instanceConfigurationLoader = false;
        })
    }
    $scope.getAllNodes = function () {
        var nodes = []
        nodeService.getAllNodes().then(function (response) {
            for (var n = 0; n < response.data.length; n++) {
                nodes.push(response.data[n].host);
            }
            $scope.nodeList = nodes;
        });


    }
    $scope.saveInstanceFile = function (instanceId) {
         $scope.instanceDetails.saveInstanceFile = true
        if ($scope.myFile) {
            var file = $scope.myFile;

            instanceService.saveFileForInstance( file,instanceId).then(function (response) {
                    $scope.closePopup('uploadInstanceFile');
                    $scope.getInstanceFile(instanceId);
            }, function error(errResponse) {
                $scope.instanceDetails.saveInstanceFile = false
                $scope.addFileToInstanceError = errResponse.data.message
            });
        }
        else {
            $scope.validationError = "Please upload the file";
        }


    }
    $scope.clearFilename=function(){
        $scope.validationError = " ";
        if ($scope.myFile) {
            $scope.myFile = null;
        }
        $scope.addFileToInstanceError=null;
        $scope.instanceStatusErrorMessage=null;
        $scope.instanceDetails.saveInstanceFile=false;
        $(".instanceFile.upload .mask").text("Upload");
    };
    $scope.getInstanceFile=function(instanceId){
        instanceService.getFilesBasedOnInstanceId(instanceId).then(function (response) {
            if(response.data.message){
                $scope.getFilesByInstanceIdError = response.data.message
                $scope.instanceDetails.loadInstanceByIdStatus = false;
            }
            else{
                $scope.instanceFiles=response.data
                $scope.instanceDetails.loadInstanceByIdStatus = false;
            }


        });


    }
    $scope.deleteInstanceFileById=function(instanceId){
        $scope.instanceDetails.showDeleteFileLoader = true;
        var fileName=instanceService.getFileName();
        instanceService.deleteInstanceFileByID(instanceId,fileName).then(function (response) {
            $scope.getInstanceFile(instanceId);
            $scope.closePopup('deleteInstanceFile');
            $scope.instanceDetails.showDeleteFileLoader = false;
        },function error(errResponse) {
            $scope.instanceDetails.showDeleteFileLoader = false;
            $scope.deleteFilesByInstanceIdError = errResponse.data.message
        });



    }
    $scope.setFileNameToService=function(fileName){
        $scope.deleteFilesByInstanceIdError = null;
        instanceService.setFileName(fileName);

    }

    function  init(){
        $scope.getAllInstanceDetails();
        $scope.getApplicationConfig();
        }
    init();




});