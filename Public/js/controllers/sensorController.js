/**
 * Created by MohammedSaleem on 01/04/16.
 */

taxiFleetControlPanel.controller("sensorController", function ($scope, $location, watchListService,
             sensorService,settingService,$rootScope,$timeout,constantService,assetApiService,assetApiUrlService ) {


    var map;
    $scope.flag = 1;
    $scope.getMapCoordinates=function(){
        settingService.getMapConfig().then(function(response){
            if(response && response.data){
                var mapDetailsObj =response.data[0];
                var map={
                    centerLatitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude,
                    centerLongitude:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude
                };
                settingService.setMapConfigObj(map);
                initMap();

            }
        })
    };
    $scope.sensorControllerObjects={/*$scope.sensorControllerObjects.addingSensor.label*/
        addingSensor:{
            value:{
                type:'Car',
            },
            type:'car',
            label:"Car",
            form:[],
            sensorTypes:[],
            isSensorNameUnique:true
        },
        markerSelected:{
            sensorName:'',
            sensorType:'',
            sensorLat:'',
            sensorLng:'',
        },
        assetConstants:'',
        paginationDetails:{
            assetPageSelected:1,
            assetTotalPageCount:0,
            maxPageNo:10,
            maxAssetsPerPage:10,
            assetTypeSelected:"all",
            "totalNoOfAssets":10
        }
    }

    //GET ASSETS TYPE
    function getConstant(){
        constantService.getConstant().then(function(result){
            $scope.sensorControllerObjects.assetConstants = result.data.constants
            sensorDetailsInit();
        })
    }

    function getInitConfig() {
        getConstant();
    }
    getInitConfig();


    //...... assets managed from the application ........

    $scope.addSensors = function(value){
        $scope.addSensorLoader=true;
        var sensorNameVariable= new Object($scope.sensor.assetName);
        $scope.sensor.sensorType=$scope.sensorControllerObjects.addingSensor.type;
        $scope.sensor.sensorLabel=$scope.sensorControllerObjects.addingSensor.label;
        $scope.sensor.assetName=$scope.sensor[sensorNameVariable];
        sensorService.addSensors($scope.sensor).then(function(result){
            console.log(result)
            if($scope.sensorControllerObjects.addingSensor.type!=='obdDevice'){
                $scope.addSensorLoader=false;
                $scope.closePopup('addSensor');
                var sensorObjAdded = result.data
                var sensorArray = $scope.sensorDataForList;
                sensorArray.unshift(sensorObjAdded)
                $scope.sensorDataForList = sensorArray;
                $scope.sensor='';
            }


            if($scope.sensorControllerObjects.addingSensor.type==='obdDevice'){
                addDeviceToApi(value.uniqueIdentifier,value.gsmId,value.modelNo)
            }
        })
    }
    function addDeviceToApi(imei,sim_number,device_model){
        var deviceConstant=constantService.getGpsDeviceWoxConstant();
        var devObj={};
        devObj.name=imei;
        devObj.imei=imei;
        devObj.object_owner=deviceConstant.object_owner;
        devObj.icon_id=deviceConstant.icon_id;
        devObj.group_id=deviceConstant.group_id;
        devObj.sim_number=sim_number;
        devObj.device_model=device_model;
        devObj.fuel_measurement_id=deviceConstant.fuel_measurement_id;
        devObj.fuel_quantity=deviceConstant.fuel_quantity;
        devObj.min_moving_speed=deviceConstant.min_moving_speed;
        devObj.min_fuel_fillings=deviceConstant.min_fuel_fillings;
        devObj.min_fuel_thefts=deviceConstant.min_fuel_thefts;
        devObj.tail_length=deviceConstant.tail_length;
        devObj.timezone_id=deviceConstant.timezone_id;
        assetApiService.addDevice(devObj).then(function (result) {
            getAllDevicesFromApiAfterAdd(imei);
        }, function error(errResponse) {
            console.log(errResponse)
        })

    }
    function getAllDevicesFromApiAfterAdd(imei){
        assetApiUrlService.getDevices().then(function (result) {
            for(var l=0;l<result.data[0].items.length;l++) {
                if(imei===result.data[0].items[l].device_data.imei){
                        sensorService.updateApiReferenceByID(result.data[0].items[l].id,imei).then(function (result) {
                            getAllDeviceFromApi()
                            getSensorsDetails()
                            $scope.addSensorLoader=false;
                            $scope.closePopup('addSensor');
                            $scope.sensor='';
                        }, function error(errResponse) {
                            console.log(errResponse)
                        })

                }
            }


        }, function error(errResponse) {
            console.log(errResponse)
        })
    }
    $scope.changeSensorType = function(type,label){
        /*$scope.sensor.sensorType=$scope.sensorControllerObjects.addingSensor.type;*/
        $scope.sensorControllerObjects.addingSensor.type=type;
        $scope.sensorControllerObjects.addingSensor.label=label;
        /*$scope.sensorControllerObjects.addingSensor.form=$scope.sensorControllerObjects.addingSensor[type].formRequired;*/
        /*if(type==='rfIdReader'){
            setRfIdValues();
        }*/
        getSensorConfiguration(type);
    }
    $scope.changeSensorType($scope.sensorControllerObjects.addingSensor.type,$scope.sensorControllerObjects.addingSensor.label);
    function getSensorsDetails(){
        sensorService.getSensors().then(function(result){
            $scope.pageLoader=false;
            $scope.sensorDataForList=result.data;

        })
    }
    $scope.setProtocolType = function(type){
        $scope.sensor.rfIdProtocol=type;
    }
    $scope.setFrequencyType = function(type){
        $scope.sensor.rfIdFrequency=type;
    }
    $scope.setRfIdtype = function(type){
        $scope.sensor.rfIdReaderType=type;
    }
    $scope.setSensorDetails = function(obj){
        sensorService.setSensorDetails(obj);
    }
    function getSensorConfiguration(sensorType){
        sensorService.getAssetTypeConfigByAssetType(sensorType).then(function(result){
            if(result.data && result.data.sensorConfiguration){
                getFormForSensorType(result.data.sensorConfiguration.configuration)
            }

        })
    }
    function getFormForSensorType(jsonData){
        sensorService.generateHtmlFromJson1(jsonData,function(data){
            $scope.sensorControllerObjects.addingSensor.form=/*$compile($sce.trustAsHtml(*/data/*))*/;
        })
    }
    function getAllSensorTypes(){
        var array  = []
        var filterArray = new Array();
        sensorService.getAllSensorTypes().then(function(result){
            if(result && result.data){
                var sensorConfigArray = result.data;
                for(var i=0;i<sensorConfigArray.length;i++){
                    var sensorConfigObj = sensorConfigArray[i];
                    array.unshift(sensorConfigObj.sensorConfiguration.sensorType);
                    filterArray.unshift(sensorConfigObj.sensorConfiguration.sensorType);
                    if(array.length==sensorConfigArray.length){
                        $scope.sensorControllerObjects.sensorTypes=array;
                        var allData = {'typeId':'all','label':'All'}
                        filterArray.unshift(allData)
                        $scope.sensorControllerObjects.sensorTypesForFilter=filterArray;
                    }
                }
                /*$scope.sensorControllerObjects.sensorTypes=result.data;*/
            }
        })
    }
    $scope.$watch('sensor.sensorName',checkSensorUniqueName);
    function checkSensorUniqueName(){
        if($scope.sensor){
            sensorService.checkSensorNameForUniqueness($scope.sensor.sensorName).then(function(result){
                $scope.sensorControllerObjects.addingSensor.isSensorNameUnique=result.data;
            })
        }
    }
    
    // ................ google maps .......
    function initMap() {
        var mapBackground='#1d212f';
        if ($("body").hasClass("goldPlatedTheme")){
            mapBackground="#ffffff";
        }
        else{

        }
        // Create a new StyledMapType
        var styledMap = new google.maps.StyledMapType($scope.mapStyles,
            {name: "Styled Map"});
        var mapCenterConfigObj=settingService.getMapConfigObj();

        var mapLatLng = {lat: mapCenterConfigObj.centerLatitude, lng: mapCenterConfigObj.centerLongitude};

        var mapOptions = {
            center: mapLatLng,
            scrollwheel: false,
            zoom: 12,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            backgroundColor: mapBackground,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };

        map = new google.maps.Map(document.getElementById('sensorMap'), mapOptions);

        //Associate the styled map with the MapTypeId and set it to display.
        if (!$("body").hasClass("goldPlatedTheme")){
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }
        else{
        }
        sensorService.setSensorMapObject(map)
    }
    $rootScope.$on('mapResize',function () {
        if(!$scope.showNotification){
            console.log("resize")
            $timeout(function () {
                google.maps.event.trigger(map, "resize");
            },1000)
        }
    });
    var showDeviceData = function (element) {


        $(".deviceIndicatorMain").removeClass("active");
        $(element).parent().addClass("active");

        var dataBox = $(".mapContainer .rightDetails");

        dataBox.fadeIn(0, function () {
            dataBox.removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
        });
        var deviceName = parseInt($(element).attr("id"));
        var devices = $scope.sensorData;

        for (var i = 0; i < devices.length; i++) {
            var deviceObj = devices[i];
            var selectedDeviceName = deviceObj.id;
            if (selectedDeviceName === deviceName) {
                $scope.sensorControllerObjects.markerSelected.lat=deviceObj.lat
                $scope.sensorControllerObjects.markerSelected.lng = deviceObj.lng;
                $scope.sensorControllerObjects.markerSelected.name = deviceObj.name;
                $scope.sensorControllerObjects.markerSelected.online = deviceObj.online;
                $scope.sensorControllerObjects.markerSelected.updatedDate = new Date(deviceObj.updatedDate);
                $scope.sensorControllerObjects.markerSelected.createdDate = new Date(deviceObj.createdDate);
                console.log($scope.sensorControllerObjects.markerSelected)
                $scope.$apply()
                break;
            }
        }
    };
    $("body").on("click", ".deviceIndicator", function (e) {
        e.stopPropagation();
        showDeviceData(this);
    });
    function getAllDeviceFromApi() {

        assetApiService.getMapPlotsFromApi().then(function (result) {

            if(result){
                plotMarkersFromApi(result)
            }


        }, function error(errResponse) {
            console.log(errResponse);
        });
    }
    function plotMarkersFromApi(deviceDetailsArray){
        for(var k=0;k<deviceDetailsArray.length;k++){
            if(deviceDetailsArray[k].lat!=0&&deviceDetailsArray[k].lng!=0){
                $scope.addMarkers(deviceDetailsArray[k].lat,deviceDetailsArray[k].lng,deviceDetailsArray[k].id)
            }

        }
    }
    $scope.addMarkers = function ( lat,lng,name) {
        var  type='camera';
        var mapPanDisableFlag=false
        var boxText = '<div class="deviceIndicatorMain"><div class="deviceIndicator ' + type +'" id="' + name + '"></div></div>';
        var myOptions = {
            content: boxText,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-15, -15),
            zIndex: null,
            enableEventPropagation: true,
            closeBoxURL: "",
            position: new google.maps.LatLng(lat, lng),
            disableAutoPan:mapPanDisableFlag
        };

        var ib = new InfoBox(myOptions);
        ib.open(map);
    };

    /*pagination*/
    $scope.getAssetDetailsByPageNo = function(pageNo){
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = pageNo;
        getSensorsDetailsByPageNoAndType();
    }
    $scope.range = function(n) {
        return new Array(n);
    };
    function range(n) {
        return new Array(n);
    };
    function getAssetCountByType(){
        var assetTypeSelectedForPagination =$scope.sensorControllerObjects.paginationDetails.assetTypeSelected;
        if(assetTypeSelectedForPagination!='all'){
            sensorService.getSensorCountByType(assetTypeSelectedForPagination).then(function(result){
                var totalPageNo = parseInt(result.data.count)/$scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
                var roundedTotalPageNo = Math.round(parseInt(result.data.count)/$scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage);
                if(totalPageNo>roundedTotalPageNo){
                    totalPageNo =roundedTotalPageNo+1;
                }else if(totalPageNo < 1){
                    totalPageNo=1;
                }else if(totalPageNo<roundedTotalPageNo){
                    totalPageNo=roundedTotalPageNo;
                }
                $scope.sensorControllerObjects.paginationDetails.totalNoOfAssets=totalPageNo;
                if(totalPageNo<=$scope.sensorControllerObjects.paginationDetails.maxPageNo){
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range(totalPageNo);
                }else{
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range($scope.sensorControllerObjects.paginationDetails.maxPageNo);
                }
                getSensorsDetailsByPageNoAndType(1,$scope.sensorControllerObjects.assetConstants.ASSET_DETAILS.DEFAULT_ASSET_NAME);
            })
        }else{
            sensorService.getSensorCount().then(function(result){
                var totalPageNo = parseInt(result.data.count)/$scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
                var roundedTotalPageNo = Math.round(parseInt(result.data.count)/$scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage);
                if(totalPageNo>roundedTotalPageNo){
                    totalPageNo =roundedTotalPageNo+1;
                }else if(totalPageNo < 1){
                    totalPageNo=1;
                }else if(totalPageNo<roundedTotalPageNo){
                    totalPageNo=roundedTotalPageNo;
                }
                $scope.sensorControllerObjects.paginationDetails.totalNoOfAssets=totalPageNo;
                if(totalPageNo<=$scope.sensorControllerObjects.paginationDetails.maxPageNo){
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range(totalPageNo);
                }else{
                    $scope.sensorControllerObjects.paginationDetails.assetTotalPageCount = range($scope.sensorControllerObjects.paginationDetails.maxPageNo);
                }
                getSensorsDetailsByPageNoAndType();
            })
        }
    }
    function getSensorsDetailsByPageNoAndType(){
        $scope.pageLoader = true;
        var assetTypeSelectedForPagination =$scope.sensorControllerObjects.paginationDetails.assetTypeSelected;
        var maxAssetPerPage = $scope.sensorControllerObjects.paginationDetails.maxAssetsPerPage;
        var selectedPageNo = $scope.sensorControllerObjects.paginationDetails.assetPageSelected;
        var skip =((selectedPageNo-1)*maxAssetPerPage)
        var limit=maxAssetPerPage;
        assetApiService.getDevicesDetails().then(function (result) {
            if(result){
                $scope.sensorData = result;
            }
        }, function error(errResponse) {
            console.log(errResponse);
        });
        if(assetTypeSelectedForPagination!='all'){
            sensorService.getAssetDetailsByPageNoAndAssetType(skip,limit,assetTypeSelectedForPagination).then(function(result){
                $scope.sensorDataForList = result.data;
                $scope.pageLoader=false;
            })
        }else{
            sensorService.getAssetDetailsByPageNo(skip,limit).then(function(result){
                $scope.sensorDataForList = result.data;
                $scope.pageLoader=false;
            })
        }

    }
    $scope.setTypeToGetAssetList = function(type){
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = 1;
        $scope.sensorControllerObjects.paginationDetails.assetTypeSelected = type;
        getAssetCountByType();
    }
    $scope.decrementPageNo = function(){
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = $scope.sensorControllerObjects.paginationDetails.assetPageSelected-1;
        getSensorsDetailsByPageNoAndType();
    }
    $scope.incrementPageNo = function(){
        $scope.sensorControllerObjects.paginationDetails.assetPageSelected = $scope.sensorControllerObjects.paginationDetails.assetPageSelected+1;
        getSensorsDetailsByPageNoAndType();
    }

    function sensorDetailsInit(){
        $scope.getMapCoordinates();
        getAssetCountByType();
        getSensorConfiguration($scope.sensorControllerObjects.assetConstants.ASSET_DETAILS.DEFAULT_ASSET_NAME);
        getAllSensorTypes();
        getAllDeviceFromApi();
        getSensorsDetails();

    }

});