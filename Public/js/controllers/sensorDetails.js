/**
 * Created by MohammedSaleem on 26/04/16.
 */

taxiFleetControlPanel.controller("sensorDetails", function ($scope,$location,watchListService,sensorService,$stateParams,
                                                            $state,settingService,$timeout,$rootScope,assetApiService,assetApiUrlService) {
    //...... Adding devices List.......

    $scope.sensorDetails={
        showPolylineLoader:false
    }
    $scope.isSensorDetailsNameUnique=true;
    function getSelectedSensorDetails(){
        $scope.sensordetails = sensorService.getSensorDetails();
    }
    $scope.setProtocolType = function(type){
        $scope.sensordetails.sensorData.rfIdProtocol=type;
    }
    $scope.setFrequencyType = function(type){
        $scope.sensordetails.sensorData.rfIdFrequency=type;
    }
    $scope.setRfIdtype = function(type){
        $scope.sensordetails.sensorData.rfIdType=type;
    }
    $scope.update = function(){
        var type = $scope.sensordetails.sensorData.sensorType;
        sensorService.getAssetNameForAssetsByTypeId(type).then(function(result){
            if(result.data && $scope.sensordetails.sensorData[result.data]){
                $scope.sensordetails.sensorData.assetName=$scope.sensordetails.sensorData[result.data];
            }
            sensorService.updateSensorDetails($scope.sensordetails).then(function(err,result){
                getSensorDetailsForEdit($stateParams.id);
            })
        })
    }
    $scope.deleteSensor = function(refId){
        console.log("*********")
        console.log(refId)
        var id= $scope.sensordetails._id;
        sensorService.deleteSensorDetailsById(id).then(function(result){
            assetApiUrlService.deleteDevice(refId).then(function(response){

                    $(".messagePopup.sensorDeleted").fadeIn(300);
                    $timeout(function () {
                        $(".messagePopup.sensorDeleted").fadeOut(300);
                        console.log("Sensor Object Deleted");
                        $state.go('necApp.sensorMain.sensors');
                    }, 3000);

            }, function error(errResponse) {
                console.log(errResponse)
            })

        })
    }
    function getSensorDetailsForEdit(id){
        sensorService.getSensorDetailsById(id).then(function(response){
            if(response && response.data){
                $scope.sensordetails = response.data;
                getSensorConfiguration($scope.sensordetails.sensorData.sensorType)
                $scope.pageLoader=false;
                if($scope.sensordetails.sensorData.latitude && $scope.sensordetails.sensorData.latitude){
                    var lat = parseFloat($scope.sensordetails.sensorData.latitude);
                    var lng = parseFloat($scope.sensordetails.sensorData.longitude);
                    var type = $scope.sensordetails.sensorData.sensorType;
                    var sensorName = $scope.sensordetails.sensorData.sensorName;

                   /* initMap(lat,lng,type,sensorName);*/
                }
                sensorService.setSensorNameOfSensorInSensorDetails(sensorName);
            }
        })

    }
    function getSensorConfiguration(sensorType){
        sensorService.getAssetTypeConfigByAssetType(sensorType).then(function(result){
            if(result.data && result.data.sensorConfiguration){
                getFormForSensorType(result.data.sensorConfiguration.configuration)
            }

        })
    }
    function getFormForSensorType(jsonData){
        sensorService.generateHtmlFromJson1(jsonData,function(data){
            $scope.form=data;
        })
    }
    $scope.$watch('sensordetails.sensorData.sensorName',checkSensorDetailsUniqueName);
    function checkSensorDetailsUniqueName(){
        var currentSensorName = sensorService.getSensorNameOfSensorInSensorDetails();
        if($scope.sensordetails
           && $scope.sensordetails.sensorData
           && $scope.sensordetails.sensorData.sensorName){
            sensorService.checkSensorNameForUniqueness($scope.sensordetails.sensorData.sensorName).then(function(result){
                if($scope.sensordetails.sensorData.sensorName === currentSensorName){
                    $scope.isSensorDetailsNameUnique = true;
                }else{
                    $scope.isSensorDetailsNameUnique=result.data;
                }
                console.log($scope.isSensorDetailsNameUnique)

            })
        }
    }



    var map;
    function initDirectionMap(mapSrcCenter,polylineArray,isMarker) {
        var mapBackground='#1d212f';
        if ($("body").hasClass("goldPlatedTheme")){
            mapBackground="#ffffff";
        }
        $scope.sensorDetails.showPolylineLoader=false;
        var  mapCenter = new google.maps.LatLng(parseFloat(mapSrcCenter.lat),parseFloat(mapSrcCenter.lng))
        var styledMap = new google.maps.StyledMapType($scope.mapStyles,{name: "Styled Map"});
        var mapOptions = {
            center: mapCenter,
            scrollwheel: false,
            zoom: 12,
            disableDefaultUI: true,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            },
            backgroundColor: mapBackground,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        };
        map = new google.maps.Map(document.getElementById('sensorDetailsMap'), mapOptions);
        //Associate the styled map with the MapTypeId and set it to display.
        if (!$("body").hasClass("goldPlatedTheme")){
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        }
        else{
        }
        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        if(isMarker===0){
            var flightPath = new google.maps.Polyline({
                path: polylineArray,
                geodesic: true,
                strokeColor: '#0000CC',
                strokeOpacity: 0.8,
                strokeWeight: 4
            });
            flightPath.setMap(map);
        }
        else if(isMarker===1){
            var  type='camera';
            var mapPanDisableFlag=false
            var boxText = '<div class="deviceIndicatorMain"><div class="deviceIndicator ' + type +'" id="' + name + '"></div></div>';
            var myOptions = {
                content: boxText,
                maxWidth: 0,
                pixelOffset: new google.maps.Size(-15, -15),
                zIndex: null,
                enableEventPropagation: true,
                closeBoxURL: "",
                position: new google.maps.LatLng(mapSrcCenter.lat,mapSrcCenter. lng),
                disableAutoPan:mapPanDisableFlag
            };

            var ib = new InfoBox(myOptions);
            ib.open(map);
        }

    }
    function getDeviceNavigationDetailsFromApi(apiRefId){
        $scope.sensorDetails.showPolylineLoader=true;
        assetApiService.getDeviceHistory(apiRefId).then(function (result) {
            var deviceDetailsArray=[];
            console.log(result.data.items)
            if(typeof result.data.items!= 'undefined'&& result &&result.data&&result.data.items.length!==0){
                var  isMarker=0;
                for(var p=0;p<result.data.items.length;p++){
                    for(var b=0;b<result.data.items[p].items.length;b++){
                        var obj={};
                        obj.lat=parseFloat(result.data.items[p].items[b].latitude);
                        obj.lng=parseFloat(result.data.items[p].items[b].longitude);
                        deviceDetailsArray.push(obj)

                    }
                }
                var mapSrcCenter=deviceDetailsArray[0];
                initDirectionMap(mapSrcCenter,deviceDetailsArray,isMarker)
                addSourceDestnationMarker(deviceDetailsArray[0].lat,deviceDetailsArray[0].lng)
                addSourceDestnationMarker(deviceDetailsArray[deviceDetailsArray.length-1].lat,deviceDetailsArray[deviceDetailsArray.length-1].lng)
            }
            else{
                assetApiUrlService.getDevices().then(function (result) {
                    $scope.sensorDetails.showPolylineLoader=false;
                    var  isMarker=1
                    for(var l=0;l<result.data[0].items.length;l++){
                        if(result.data[0].items[l].id===parseInt(apiRefId)){

                            var obj={};
                            obj.lat=parseFloat(result.data[0].items[l].lat);
                            obj.lng=parseFloat(result.data[0].items[l].lng);


                            initDirectionMap(obj,[],isMarker)

                        }
                    }

                }, function error(errResponse) {
                    console.log(errResponse);
                });

            }

        }, function error(errResponse) {
            console.log(errResponse);
        });
    }
    function addSourceDestnationMarker(lat,lng){
        var marker = new google.maps.Marker({
            position:  {lat: lat, lng: lng},
            map: map
        });

    }
    $scope.addMarkers = function ( lat,lng,name) {
        var  type='camera';
        var mapPanDisableFlag=false
        var boxText = '<div class="deviceIndicatorMain"><div class="deviceIndicator ' + type +'" id="' + name + '"></div></div>';
        var myOptions = {
            content: boxText,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-15, -15),
            zIndex: null,
            enableEventPropagation: true,
            closeBoxURL: "",
            position: new google.maps.LatLng(lat, lng),
            disableAutoPan:mapPanDisableFlag
        };

        var ib = new InfoBox(myOptions);
        ib.open(map);
    };
    $rootScope.$on('mapResize',function () {
        if(!$scope.showNotification){
            $timeout(function () {
                google.maps.event.trigger(map, "resize");
            },1000)
        }
    });
    function calculateAndDisplayRoute(src, dest) {

        // Instantiate a directions service.
        var  directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({
            map: map
        });
        var  directionSrc = new google.maps.LatLng(src.lat,src.lang),
            directionDest = new google.maps.LatLng(dest.lat,dest.lang);
        directionsService.route({
            origin: directionSrc,
            destination: directionDest,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                console.log('Directions request failed due to ' + status);
            }
        });
    }
    $scope.setDefaultMapCoordinates=function(){
        settingService.getMapConfig().then(function(response){
            if(response && response.data){
                var mapDetailsObj =response.data[0];
                var map={
                    lat:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.latitude,
                    lng:mapDetailsObj.SettingsConfiguration.mapSettings.mapCenter.longitude
                };

                initDirectionMap(map,[],'');

            }
        })
    };
    if($stateParams.id){
        if($stateParams.apiRefId){
            getDeviceNavigationDetailsFromApi($stateParams.apiRefId);
            getSensorDetailsForEdit($stateParams.id);
        }
        else{
            getSensorDetailsForEdit($stateParams.id);
            $scope.setDefaultMapCoordinates()

        }

    }

});