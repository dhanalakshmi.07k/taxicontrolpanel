/**
 * Created by MohammedSaleem on 12/11/15.
 */

$(document).ready(function () {

    var parentEle = $("body");
    fun = {
        notificationList: function () {
            parentEle.on("click", "#appHeader .notificationBtn", function (e) {
                e.stopPropagation();
                var init = $(this).data("init");

                // ...... UI style based function .......
                if ($("body").hasClass("arabicUIStyle")){
                    if (init == '0') {

                        $("#contWrapper #pageCont").animate({
                            marginLeft: "320px"
                        }, 300, function () {
                        });

                        setTimeout(function () {
                            $(".notifications").fadeIn(0, function () {
                                $(".notifications").removeClass("bounceOutLeftCustom").addClass("bounceInLeftCustom");
                            });
                        }, 200);

                        $(this).data({
                            'init': '1'
                        }).addClass("active");

                    }
                    else {

                        $(".notifications").removeClass("bounceInLeftCustom").addClass("bounceOutLeftCustom");

                        setTimeout(function () {
                            $("#contWrapper #pageCont").animate({
                                marginLeft: "0"
                            }, 300);
                        }, 300);

                        $(this).data({
                            'init': '0'
                        }).removeClass("active");

                        setTimeout(function () {
                            $(".notifications").fadeOut(0)
                        }, 500);
                    }
                }
                else {
                    if (init == '0') {

                        $("#contWrapper #pageCont").animate({
                            marginRight: "320px"
                        }, 300, function () {
                        });

                        setTimeout(function () {
                            $(".notifications").fadeIn(0, function () {
                                $(".notifications").removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
                            });
                        }, 200);

                        $(this).data({
                            'init': '1'
                        }).addClass("active");

                    }
                    else {

                        $(".notifications").removeClass("bounceInRightCustom").addClass("bounceOutRightCustom");

                        setTimeout(function () {
                            $("#contWrapper #pageCont").animate({
                                marginRight: "0"
                            }, 300);
                        }, 300);

                        $(this).data({
                            'init': '0'
                        }).removeClass("active");

                        setTimeout(function () {
                            $(".notifications").fadeOut(0)
                        }, 500);
                    }
                }



            });


            parentEle.on("click", ".notificationList li", function () {
                var init = $(this).data("init");

                if (init == '0') {
                    $(this).find(".actionBtn").slideDown(200);
                    $(this).data({
                        'init': '1'
                    });
                }
                else {
                    $(this).find(".actionBtn").slideUp(200);
                    $(this).data({
                        'init': '0'
                    })
                }
            });

            parentEle.on("click", ".notificationList li .actionBtn .btn", function () {
                $(".notificationList li .actionBtn").slideUp(200);
                $(".notificationList li").data({
                    'init': '0'
                });
                $(this).parents().eq(2).addClass("notified");
            });

        },
        help: function () {
            parentEle.on("click", ".needHelpBtn", function (e) {
                e.stopPropagation();
                var init = $(this).data("init");

                if ($("body").hasClass("arabicUIStyle")){
                    if (init == '0') {
                        $(".helpBar").fadeIn(0, function () {
                            $(".helpBar").removeClass("bounceOutLeftCustom").addClass("bounceInLeftCustom");
                        });
                        $(this).data({
                            'init': '1'
                        }).text("HIDE HELP");
                    }
                    else{
                        $(".helpBar").removeClass("bounceInLeftCustom").addClass("bounceOutLeftCustom");
                        setTimeout(function () {
                            $(".helpBar").fadeOut(0)
                        }, 500);
                        $(this).data({
                            'init': '0'
                        }).text("SHOW HELP");

                    }
                }
                else {
                    if (init == '0') {
                        $(".helpBar").fadeIn(0, function () {
                            $(".helpBar").removeClass("bounceOutRightCustom").addClass("bounceInRightCustom");
                        });
                        $(this).data({
                            'init': '1'
                        }).text("HIDE HELP");
                    }
                    else{
                        $(".helpBar").removeClass("bounceInRightCustom").addClass("bounceOutRightCustom");
                        setTimeout(function () {
                            $(".helpBar").fadeOut(0)
                        }, 500);
                        $(this).data({
                            'init': '0'
                        }).text("SHOW HELP");

                    }
                }
            });
            parentEle.on("click", ".helpBar .x", function (e) {
                $(".helpBar").removeClass("bounceInRightCustom").addClass("bounceOutRightCustom");
                setTimeout(function () {
                    $(".helpBar").fadeOut(0)
                }, 500);

                $(".needHelpBtn").data({
                    'init': '0'
                }).text("NEED HELP");
            });
        },
        dropDown: function () {
            parentEle.on("click", ".dropDown .headTitle ,.multiSelect .headTitle", function (e) {
                e.stopPropagation();
                var init = $(this).parent().data("init");
                if (init == "0") {
                    $(".dropDown .list, .multiSelect .list").slideUp(100);
                    $(".dropDown .head, .multiSelect .head").data({
                        "init": "0"
                    });
                    $(this).parents().eq(1).find(".list").slideDown(200);
                    $(this).parent().data({
                        "init": "1"
                    })
                }
                else {
                    $(this).parents().eq(1).find(".list").slideUp(200);
                    $(this).parent().data({
                        "init": "0"
                    })
                }
            });
            parentEle.on("click", ".dropDown .list li", function (e) {
                var value = $(this).text().trim();
                $(this).parents().eq(1).find(".headTitle").focus();
                $(this).parents().eq(1).find(".headTitle").text(value);
                $(this).parents().eq(1).find(".headTitle").val(value);

                $(this).parent().slideUp(200);
                $(this).parents().eq(1).find(".head").data({
                    "init": "0"
                })
            });
            parentEle.on("click", ".multiSelect .list li", function (e) {
                e.stopImmediatePropagation();
                var that = this;
                var listHeader = $(this).parents().eq(1).find(".headTitle");
                var selectedVal = "";
                setTimeout(function () {
                    selectionLen = $(that).parent().find("input[type='checkbox']:checked").length;

                    if (selectionLen == 0) {
                        listHeader.text("Please select");
                    }
                    else {
                        $(that).parent().find("input[type='checkbox']:checked").each(function () {

                            selectedVal = selectedVal + $(this).parents().eq(1).find(".listName").text().trim() + ", ";
                        });
                        listHeader.text(selectedVal);
                        console.log(selectedVal);
                    }


                }, 50)

            });
        },
        popup: function (btn, popup) {
            parentEle.on("click", btn, function (e) {
                e.stopPropagation();
                $(".background").fadeIn(250, function () {
                    $(popup).fadeIn(250);
                });
            });

            parentEle.on("click", ".popup .close", function (e) {
                e.stopPropagation();
                $(".popup").fadeOut(250, function () {
                    $(".background").fadeOut(250);
                });
            });
        },
        messageBox: function (btn, msgBox) {
            var setTime;
            parentEle.on("click", btn, function (e) {
                $(".messagePopup").fadeOut(100);
                clearTimeout(setTime);
                $(msgBox).fadeIn(200);
                setTime=setTimeout(function () {
                    $(msgBox).fadeOut(200);
                }, 3000);
            });
        },
        accordion: function () {
            parentEle.on("click", ".accordionSec .accordionHead", function (e) {
                var init=$(this).data("init");
                if (init=="0"){
                    $(this).parent().addClass("expand");
                    $(this).parent().find("> .accordionContent").slideDown(300);
                    $(this).data({
                        "init":"1"
                    });
                    $(this).find(".expandBtn").text("-");
                }
                else{
                    $(this).parent().removeClass("expand");
                    $(this).parent().find(".accordionContent").slideUp(300);
                    $(this).data({
                        "init":"0"
                    });
                    $(this).find(".expandBtn").text("+");


                    $(this).parent().find(".accordionSec").removeClass("expand");
                    $(this).parent().find(".expandBtn").text("+");
                    $(this).parent().find(".accordionSec .accordionHead").data({
                        "init":"0"
                    });
                }
            });
        },
        closeMapData: function () {
            parentEle.on("click", ".mapContainer .close", function (e) {

                $(".mapContainer .rightDetails").removeClass("bounceInRightCustom").addClass("bounceOutRightCustom");
                setTimeout(function () {
                    $(".mapContainer .rightDetails").fadeOut(0)
                }, 450);

            });
        },
        selectLinkAsset: function () {
            var slideCounter=0;
            function dataSliderAnimation(container,index) {
                var marker=container.find(".marker");
                marker.animate({
                    left: index*(marker.width())
                },200)
            }
            parentEle.on("click", ".linksListTable .rowBox .icon:not(.connectionIcon)", function (e) {
                $(".rowBox").removeClass("active");
                $(this).parents().eq(3).addClass("active")
                    .find(".basicDetails").slideDown(300);
                $(".rowBox:not(.active)").find(".basicDetails").slideUp(300);

                var marker=$(this).parents().eq(1).find(".marker");
                var container= $(this).parents().eq(1);

                container.find("li").removeClass("active");
                $(this).parent().addClass("active");

                var index= container.find("li.active").index();

                slideCounter=index;

                // console.log(slideCounter)
                dataSliderAnimation(container,index);
            });

            parentEle.on("click", ".nextAssetDetailsBtn", function (e) {

                console.log(slideCounter)
                slideCounter=slideCounter+2;
                var container= $(".linksListTable.newAssetTable .rowCont.rowDetails");

                console.log(slideCounter)
                dataSliderAnimation(container,slideCounter);
            });

            parentEle.on("click", ".prevAssetDetailsBtn", function (e) {

                console.log(slideCounter)
                slideCounter=slideCounter-2;

                var container= $(".linksListTable.newAssetTable .rowCont.rowDetails");
                console.log(slideCounter)
                dataSliderAnimation(container,slideCounter);
            });


        },
        defaultClick: function () {
            $(document).click(function () {
                $(".dropDown .list, .multiSelect .list").slideUp(200);
                $(".mapMenu").slideUp(200);
                $(".dropDown .head, .multiSelect .head").data({
                    "init": "0"
                });

                $(".notificationList .actionBtn").slideUp(200);
                $(".notificationList li").data({
                    'init': '0'
                });

                // $(".popup").fadeOut(250, function () {
                //     $(".background").fadeOut(250);
                // });

                $(".mapContainer .rightDetails").removeClass("bounceInRightCustom").addClass("bounceOutRightCustom");
                setTimeout(function () {
                    $(".mapContainer .rightDetails").fadeOut(0);
                }, 450);
                $(".deviceIndicatorMain").removeClass("active");

                $(".rowBox").removeClass("active");
                $(".rowBox:not(.active)").find(".basicDetails").slideUp(300);

            })
        },
        preventDefaultClicks: function () {
            var selectors = 'input,.notificationList, .notificationList .actionBtn, .popup,.personPic .deleteBtnMain,.map, .mapContainer .rightDetails, .bottomOpt, .rowBox';
            var preventDefault='.instanceBtn';
            parentEle.on("click", selectors, function (e) {
                e.stopImmediatePropagation();
                e.stopPropagation();
            });
            parentEle.on("click", preventDefault, function (e) {
                e.preventDefault();
            });
        }
    };
    fun.notificationList();
    fun.help();
    fun.dropDown();
    fun.popup(".addPeopleBtn", ".popup.addPeopleSec");
    fun.popup(".addSensorBtn", ".popup.addSensor");
    fun.popup(".addDriverBtn", ".popup.addSensor");
    fun.popup(".addInstanceBtn", ".popup.addInstance");
    fun.popup(".addServerBtn", ".popup.addServer");
    fun.popup(".cloneInstanceBtn", ".popup.cloneInstance");
    fun.popup(".deleteInstanceBtn", ".popup.deleteInstance");
    fun.popup(".addNewAppBtn", ".popup.addNewApp");
    fun.popup(".uninstallAppBtn", ".popup.uninstallApp");
    fun.popup(".createUserBtn,.userTable .editBtn", ".popup.addNewUser");
    fun.popup(".userTable .deleteBtn", ".popup.deleteUser");
    fun.popup(".addNewHelpWidgetBtn,.editHelpWidgetBtn", ".popup.addNewHelpWidget");
    fun.popup(".deleteHelpWidgetBtn", ".popup.deleteHelpWidget");
    fun.popup(".configDetailsOfInstance", ".popup.configDetailsOfInstance");
    fun.popup(".sensorDelete", ".popup.deleteSensor");
    fun.popup(".deleteLink", ".popup.deleteLink");
    fun.popup(".uploadInstanceFileBtn", ".popup.uploadInstanceFile");
    fun.popup(".deleteInstanceFileBtn", ".popup.deleteInstanceFile");
    fun.popup(".startInstanceBtn", ".popup.startInstance");
    fun.popup(".stopInstanceBtn", ".popup.stopInstance");
    fun.popup(".nextAssetDetailsPopUpBtn,.prevAssetDetailsPopUpBtn", ".popup.nextAssetDetailsPopup");
    fun.messageBox(".mapSettingsBtn",".messagePopup.mapSettings");
    // fun.messageBox(".instanceBtn.start",".messagePopup.instanceStarted");
    fun.accordion();
    fun.closeMapData();
    fun.selectLinkAsset();
    fun.defaultClick();
    fun.preventDefaultClicks();
});
