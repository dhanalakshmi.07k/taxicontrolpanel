## Application

Queues of people form in various situations and locations in a queue area. Queue can structured or unstructured. In case of structured queue, people form a queue in a fixed, predictable position, such as at supermarket checkouts, some other retail locations such as banks, airport security and so on. In un-structured queue, people form a queue in unpredictable and varying locations and directions. This is often the case in some forms of retail, taxi queues, ATMs and at periods of high demand in many situations.

These are variety of measurement technologies which measure and predict queue lengths and waiting times and provide management information to help service levels and resource deployment.

Organizations are interested in various queue metrics such as

* Number of people entering the queue
* Queue Length
* Average Wait Time
* Till Boarding/servicing Time
* Total Wait Time

Number of ways the organizations inform the people waiting in the queue or perform operations analytics is as follows

* Inform customers of waiting time
* Demand prediction/forecasting
* Increased customer support
* Manage people flow, by increased customer support personnel

## Description

Bus interchanges in major metropolis have multiple loading berths. Each loading berth can have buses for different routes. People will queue in any of the lane in any loading berth depending upon travel patterns. Since loading berths and lanes are close to each other, understanding customer behavior is key. 

This queue fill level application, emits queue fill level information in percentage (%) for each lane for all berths. Depending up height where the sensor is installed, one or more sensor might be needed for full coverage of the area of observation.

In case of Woodlands Bus interchange in Singapore, 2 sensors are needed to monitor each berth. These 2 sensors will be able to monitor all loading lanes aggregated in that corresponding berth

## Sensors

Queue fill level analytics runs different video analytics algorithms depending upon the camera sensor used. Possible sensor choices include

* IP Cameras
* 3D Camera sensors

IP Camera based video analytics for queue fill level detection is performed using feature extraction and matching techniques.

![Sensor](images/ipcamera.png)

Various considerations needed for selecting the IP camera sensor include 
- Blurry images, particularly motion blur.
- Site having poor and varying lighting conditions, low contrast due to overexposure, reflection or shadows.
- Glare, environmental conditions

3D camera sensor uses pair of camera to build 3D depth model of the scene. From that model, human sized elevations are selected and tagged as humans. There can be two possible count and analytics can be performed.

* Trip wire count
* Zonal Count

![Censys3d](images/censys3d.png)

In case of Woodlands Bus Interchange in Singapore, regular IP cameras are used. 

For CCK and Yishun Interchange in Singapore, 3D sensors are used.

These IP cameras are powered by PoE switch, wherein power is supplied via ethernet cable itself.

## Input and Output Streams

Video information from IP camera are encoded while streamed through IP network. There are two basic types of compression: 

* **Frame-by-frame**
  - Example is MJPEG, takes a full picture for each frame.
* **Temporal**
  - Example is H.264 and MPEG4, periodically takes a full picture and uses sophisticated algorithms to interpret what is happening between those full picture frames.

**MPEG4** is used for low-quality, low-bandwidth video while **MJPEG** and **H.264** work with higher quality HD/Megapixel video. Since we are primarily talking about surveillance applications where you need to capture detail, we will limit this article to the higher-quality MJPEG and H.264 compressions.

For example, in case of Singapore Woodlands Temporary Bus Interchange, we use MJPEG stream from IP camera.

## Installation

### Physical sensor installation

Sensor used for ANPR, IP cameras in this case are mounted on the ceiling facing exactly down. The height of the sensor has to be such that it is just enough to have clear and un-obstructed view observed region. Various considerations to find right location for sensor installation are

* Perpendicular to ground plane
* Obstructions
* Vibration
* Environmental conditions affecting the camera cover
* Vandalism

Actual installation of IP camera used for queue level detection purposes in Singapore Woodlands Temporary Bus Interchange is in the following picture.

![Queue Camera](images/queue-level-camera.jpg)

Height of installation of the camera sensor depends on the focal length, horizontal and vertical FOV etc.,


### Sensor installation in Control Panel

Steps to install IP Camera based sensor is described as follows

> Goto **Homepage** in Intelligent Video Solutions (IVS) Control Panel.
> 
> Click **Sensors tab** in which is present in the Tab in the left side.
>
> Click **New Sensor** button.
> 
> Click **Sensor Type** Combo box and select **Camera** option
> 
> Assign a unique **Sensor Name**
> 
> Enter the sensor's **Latitude** and **Longitude** coordinates in the form boxes
> 
> Click **Add** to complete the sensor installation
> 
> The newly added sensor is now listed in the **Sensor List** view

The user can edit or delete the sensor by selecting the appropriate sensor from sensor list. 

The map view in the **Sensors Page** shows the sensor icon on the map.

### Application installation

The Queue Occupancy Detection application is installation steps is as follows

> Goto **Homepage** in Intelligent Video Solutions (IVS) Control Panel.
> 
> Click **Apps tab** in which is present in the Tab in the left side.
>
> Click **New App** button.
> 
> Select the provided **Binary Zip File**.

## Input

Streams from various IP cameras are transmitted to the on-site datacenter through system of ethernet switches, repeaters. The cameras are powered through PoE. Each camera is tilted, rotated and panned to look at the region of interest where the queue is expected to be there.

### Input Source

Address where the camera stream is available can be in any of the following formats

```
rtsp://[camera-ip-address]:554/axis-media/media.amp
```

```
rtsp://[camera-ip-address]:554/axis-media/media.amp?videocodec=h264&compression=90
```

```
rtsp://ip_adx/MediaInput/h264
```

```
rtsp://[login:password@][camera domain name or IP address]/media/video1
```

### Input data format

In case of Singapore Woodlands Temporary Bus Interchange, we use MJPEG stream from IP camera. In MJPEG format, each frame in the video stream is I-Frame. The nice thing about MPEG is that we can run the frame rate as low as 5 fps or even 1 fps and get exceptional images every time.

![MJPEG](images/mjpeg-format.jpg)

During image processing, each image can be fetched from the stream for video analytics purpose to extract license plates.

## Output

The Queue Occupancy Detection application emits queue fill levels through message queue. These messages drive dashboard hosted command center in the bus interchange. These messages can also be streamed to the operational analytics system.

### Output data format

The data is sent as ZeroMQ message using PUSH to output consumer. The example of typical consumer can be **web dashboard** , outdoor **display panels** etc.

### Dashboard

Output data from this application can be viewed in application specific dashboard, similar to the one hosted in 

```
http://50.17.231.31:4000/#/app/analyticsAll
```

The dashboards are displayed in over-sized display panels in command center in bus interchanges.

![DISPLAY](images/dashboard-display.jpg)

## Settings

This application needs a few settings or fields to be configured. The configuration is specified as JSON object and its sample is specified below.

``` javascript
{
  "berth": 2,
  "entranceCamera": "berth2_entrance.mp4",
  "exitCamera": "berth2_exit.mp4",
  "redQueueEntranceMask": "berth2_red_entrance.png",
  "redQueueExitMask": "berth2_red_exit.png",
  "blueQueueEntranceMask": "berth2_blue_entrance.png",
  "blueQueueExitMask": "berth2_blue_exit.png",
  "greenQueueEntranceMask": "berth2_green_entrance.png",
  "greenQueueExitMask": "berth2_green_exit.png",
  "priorityQueueMask": "berth2_priority.png",
  "outputAddress": "tcp://*:7892"
}
```

In the above configuration format, the description for the fields are as follows.

> "**berth**": "Loading berth number.",
>
> "**entranceCamera**": "Camera positioned at the entrance. In case of connectivity to camera, refer to *Input Types* section
>
> "**exitCamera**": "Camera positioned at the exit.". n case of connectivity to camera, refer to *Input Types* section
>
> "**redQueueEntranceMask**": "Image mask defining the \"red\" queue area seen from the entrance camera.",
>
> "**redQueueExitMask**": "Image mask defining the \"red\" queue area seen from the entrance camera.",
>
> "**greenQueueEntranceMask**": "Image mask defining the \"green\" queue area seen from the entrance camera.",
>
> "**greenQueueExitMask**": "Image mask defining the \"green\" queue area seen from the entrance camera.",
>
> "**blueQueueEntranceMask**": "Image mask defining the \"blue\" queue area seen from the entrance camera.",
>
> "**blueQueueExitMask**": "Image mask defining the \"blue\" queue area seen from the entrance camera.",
>
> "**priorityQueueMask**": "Image mask defining the priority queue area seen from the exit camera.",
>
> "**outputAddress**": "Address of the ZMQ PULL socket where to send the output.",


