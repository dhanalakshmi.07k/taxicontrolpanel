## Application

Wireless vehicle detection sensors efficiently measure parking occupancy. Sensors mounted in the surface of individual parking spots detect vehicle presence and send that information to a central server.

Parking occupancy information can be used to guide drivers to available unloading berths or parking spaces, to increase traffic flow bus interchanges. Parking utilization and unloading efficiency are optimized.

### Description
Bus interchanges in various metropolis are very busy locations. There are various functional areas within the interchange. The areas can be

* Unloading Berths
* Parking Bays
* Loading Berths

A typical bus interchange has handful of **unloading berths** (5 to 10), where the arriving vehicles (buses) unload the passenger transiting through the interchange. Efficient utilization of unloading berths is needed to facilitate smoother unloading of passengers. Vehicle occupancy sensors are placed in the unloading berths to find out the occupancy levels of these berths and inform the incoming drivers of available unloading berths.

Once the passengers are unloaded, the buses are moved to the **parking spaces**, and parked till the next scheduled departure time. During peak hours, it takes quite a bit of time for the drivers to find parking spot for bus. It might take quite a bit of iterations or rounds by the driver to go around before he/she finds the parking space. Parking sensors are placed in all the parking bays, to find out occupancy levels in different parking zones. These occupancy levels are displayed in huge display panels in the interchange.

### Sensors

Currently supported sensor is **SENSIT** from Nedap. SENSIT sensors can easily be implemented into any existing parking management system and rapid deployment is guaranteed. The SENSIT IR is a vehicle detection sensor featured with dual detection technology allowing for vandal resistant installation into a parking space. 

![Sensor](images/sensor.png)

Key features of SENSIT sensor is as follows.

* Wireless detection and communication
* Easy installation without wiring
* Efficient parking occupancy measurement and overstay detection
* Flexible integration with parking applications
* Dual detection *(infrared and earth magnetic field)*
* Vandal resistant

The sensor has battery life of 5 to 10 years. It is protected by IP67 rated sealed housing. 

### Input and Output Streams

The sensors are read only.  The actual status (occupancy) of the sensor is transmitted to the Relay Node, which is part of the wireless mesh network. The SENSIT sensors features earth magnetic field and infrared detection. The combined detection effectively detects
vehicles using a sophisticated algorithm to ensure detection is invulnerable to snow, dirt and leafs.

The sensor stream consists of 

```
Sensor Status,
Battery Level,
Update date and time
```
## Installation

### Physical Sensor Installation
Vehicle detection sensor is mounted flush with the road surface. A hole of about 3 inch diameter and 6 inch depth needs to be drilled to place the sensor. The sensor is resistant to snow ploughs.

* earth magnetic detection
* snowplough resistant
* vandal resistant mounting

One relay node for 50 sensors is needed. Maximum range for these sensor's wireless transmission is about 100 meters.

### Sensor Installation in control panel
All the sensor's data streams are aggregated in the aggregation server, which publishes unfied view of all sensors. The path to this mount point is specified in the configration setting of the application instance.

### Application Installation

The parking occupancy application is installation steps is as follows


> Goto **Homepage** in Intelligent Video Solutions (IVS) Control Panel.
> 
> Click **Apps tab** in which is present in the Tab in the left side.
>
> Click **New App** button.
> 
> Select the provided **Binary Zip File**.

## Input

Occupancy information from individual sensors are relayed using relay node to central server. The sensors measure occupancy using magnetic field and infra-red measurements.
 
### Input source

The address where the JSON formatted sensor information can be pulled from is in

[Sample Sensor Stream Link](http://119.73.147.2/smrt/parkingbaystatus.php)

```
http://119.73.147.2/smrt/parkingbaystatus.php 
```


### Input data format

The data is fomatted as array of JSON objects. Sample data format is specified below.

```javascript
[
{"date":"20160716","time":"134223","bayid":"A1","status":"0","batterylevel":"3.5","updatedate":"20160201","updatetime":"173253"},
{"date":"20160716","time":"134223","bayid":"A7","status":"0","batterylevel":"3.5","updatedate":"20160201","updatetime":"175231"},
{"date":"20160716","time":"134223","bayid":"A6","status":"0","batterylevel":"3.5","updatedate":"20160201","updatetime":"170915"},
{"date":"20160716","time":"134223","bayid":"A5","status":"0","batterylevel":"3.5","updatedate":"20160201","updatetime":"171809"},
{"date":"20160716","time":"134223","bayid":"A4","status":"0","batterylevel":"3.5","updatedate":"20160201","updatetime":"174917"},
{"date":"20160716","time":"134223","bayid":"A3","status":"2","batterylevel":"3.5","updatedate":"19700101","updatetime":"010000"}
]
```
## Output

The pulse parking sensor application receives the parking data informtion and emits to the dashboard hosted in web server.

### Output data format

The data is sent as ZeroMQ message using PUSH to output consumer. The example of typical consumer can be **web dashboard** , outdoor **display panels** etc.

### Dashboard

Output data from this application can be viewed in application specific dashboard, similar to the one hosted in 

```
http://50.17.231.31:4000/#/app/analyticsAll
```

## Settings

This application needs a few settings or fields to be configured. The configuration is specified as JSON object and its sample is specified below.

``` javascript
  "configuration": {
    "sensorsUrl": "http://119.73.147.2/smrt/parkingbaystatus.php",
    "outputAddress": "http://50.17.231.31:4000",
    "refreshRate": 30
    }
```

> "sensorsUrl" specifies the Url for retrieving the sensors status
> 
> "outputAddress" specifies the address of the ZMQ PULL socket where to send the output
> 
> "refreshRate" specifies refresh rate (in seconds) for updating the sensors status

