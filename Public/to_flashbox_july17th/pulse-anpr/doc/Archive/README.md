## Application

**Automatic Number Plate Recognition** (or as frequently called 'number plate recognition', **ANPR**) is a special form of optical character recognition (OCR). ANPR is a type of software technology, that enables computer systems to read automatically the registration number (license number) of vehicles from digital pictures. Reading automatically the registration number means transforming the pixels of the digital image into the ASCII text of the number plate.

It can use existing closed-circuit television, road-rule enforcement cameras, or cameras specifically designed for the task. ANPR is used by police forces around the world for law enforcement purposes, including to check if a vehicle is registered or licensed. It is also used for electronic toll collection on pay-per-use roads and as a method of cataloging the movements of traffic for example by highways agencies.

From ANPR point of view the image quality is a key factor. Capturing of fast moving vehicles needs special technique to avoid motion blur which can decrease the recognition accuracy dramatically. 

It uses a series of image manipulation techniques to detect, normalize and enhance the image of the number plate, and then optical character recognition (OCR) to extract the alphanumerics of the license plate.

Various **components** in ANPR include

- Plate localization
  - Responsible for finding and isolating the plate on the picture.
- Plate orientation and sizing
  - Compensates for the skew of the plate and adjusts the dimensions to the required size.
- Adaptive normalization 
  - Adjusts the brightness and contrast of the image.
- Character segmentation
  - Finds the individual character image segments on the plates.
- Optical character recognition
  - Recognizes individual characters corresponding to the locale's character and font specifications 
-  Syntactical analysis
  -  Checks characters and positions against country-specific rules.
- The averaging of the recognised value over multiple fields/images to produce a more reliable or confident result.

## Description

In most major metropolis bus interchanges, there are many buses roll in and out every minute. There can be multiple entry and exit lane in every interchange. It is key to know the arrival and departure of vehicles in and out of interchange in realtime. This information is needed to

* Manage assets
* Infer driver efficiency
* Schedule planning
* Operational efficiency

ANPR systems can be deployed in one of two basic approaches: 

* The system where the entire process to be performed at the lane location in real-time.
* The system which transmits all the images from one or more lanes to a remote computer location and performs the ANPR process there in realtime or in the future as batch process.

When done at the lane site, the information captured of the plate alphanumeric, date-time, lane identification, and any other information required is completed in approximately 50 to 250 milliseconds depending upon speed of vehicles in the observed region. This information can easily be transmitted to a remote computer for further processing if necessary, or stored at the lane for later retrieval. 

In the other arrangement, there are typically large numbers of PCs used in a server farm to handle high workloads. Often in such systems, there is a requirement to forward all images or video to the remote server farm or datacenter, and this can require larger bandwidth transmission media.

A typical interchange in Singapore has about 1 or 2 entry lanes and 1 or 2 exit lanes. One sensor for each lane is deployed on site.

## Sensors

ANPR uses optical character recognition on images taken by cameras. The cameras used can be existing road-rule enforcement or closed-circuit television cameras, as well as mobile units, which are usually attached to vehicles. If day and night and all weather operation is required, special infrared cameras are needed to take a clearer image of the plates.

Various considerations needed for selecting the sensor include 
- Blurry images, particularly motion blur.
- Site having poor and varying lighting conditions, low contrast due to overexposure, reflection or shadows.
- Headlight glare, environmental conditions

In case of interchanges in Singapore, regular IP cameras are used. 

![Sensor](images/ipcamera.png)

These IP cameras are powered by PoE switch, wherein power is supplied via ethernet cable itself.

## Input and Output Streams

Video information from IP camera are encoded while streamed through IP network. There are two basic types of compression: 

* **Frame-by-frame**
  - Example is MJPEG, takes a full picture for each frame.
* **Temporal**
  - Example is H.264 and MPEG4, periodically takes a full picture and uses sophisticated algorithms to interpret what is happening between those full picture frames.

**MPEG4** is used for low-quality, low-bandwidth video while **MJPEG** and **H.264** work with higher quality HD/Megapixel video. Since we are primarily talking about surveillance applications where you need to capture detail, we will limit this article to the higher-quality MJPEG and H.264 compressions.

For example, in case of Singapore Woodlands Temporary Bus Interchange, we use MJPEG stream from IP camera

## Installation

### Physical sensor installation

Sensor used for ANPR, IP cameras in this case are mounted on a pole. The height of the sensor has to be such that it is just enough to have clear and un-obstructed view observed region. Much higher angle of view to the observed plane or region will cause highly sqewed license plate characters which are difficult to recognize. Various considerations to find right location for sensor installation are

* Backlight
* Obstructions
* Vibration
* Environmental conditions affecting the camera cover
* Height of pole
* Vandalism

Actual installation of IP camera used for ANPR purposes, in Singapore Woodlands Temporary Bus Interchange is in the following picture.

![ANPR Camera](images/anpr-camera.jpg)

### Sensor installation in Control Panel

Steps to install IP Camera based sensor is described as follows

> Goto **Homepage** in Intelligent Video Solutions (IVS) Control Panel.
> 
> Click **Sensors tab** in which is present in the Tab in the left side.
>
> Click **New Sensor** button.
> 
> Click **Sensor Type** Combo box and select **Camera** option
> 
> Assign a unique **Sensor Name**
> 
> Enter the sensor's **Latitude** and **Longitude** coordinates in the form boxes
> 
> Click **Add** to complete the sensor installation
> 
> The newly added sensor is now listed in the **Sensor List** view

The user can edit or delete the sensor by selecting the appropriate sensor from sensor list. 

The map view in the **Sensors Page** shows the sensor icon on the map.

### Application installation

The ANPR application is installation steps is as follows

> Goto **Homepage** in Intelligent Video Solutions (IVS) Control Panel.
> 
> Click **Apps tab** in which is present in the Tab in the left side.
>
> Click **New App** button.
> 
> Select the provided **Binary Zip File**.

## Input

Streams from various ANPR cameras are transmitted to the on-site datacenter through system of ethernet switches, repeaters. The cameras are powered through PoE. Each camera is tilted, rotated and panned to look at the region of interest where the vehicle license plates are expected to be there.

### Input Source

Address where the camera stream is available can be in any of the following formats

```
rtsp://[camera-ip-address]:554/axis-media/media.amp
```

```
rtsp://[camera-ip-address]:554/axis-media/media.amp?videocodec=h264&compression=90
```

```
rtsp://ip_adx/MediaInput/h264
```

```
rtsp://[login:password@][camera domain name or IP address]/media/video1
```

### Input data format

In case of Singapore Woodlands Temporary Bus Interchange, we use MJPEG stream from IP camera. In MJPEG format, each frame in the video stream is I-Frame. The nice thing about MPEG is that we can run the frame rate as low as 5 fps or even 1 fps and get exceptional images every time.

![MJPEG](images/mjpeg-format.jpg)

During image processing, each image can be fetched from the stream for video analytics purpose to extract license plates.

## Output

The ANPR application emits recognized plate information as message queue. These messages drive dashboard hosted command center in the bus interchange. These messages can also be streamed to the operational analytics system.

### Output data format

The data is sent as ZeroMQ message using PUSH to output consumer. The example of typical consumer can be **web dashboard** , outdoor **display panels** etc.

### Dashboard

Output data from this application can be viewed in application specific dashboard, similar to the one hosted in 

```
http://50.17.231.31:4000/#/app/analyticsAll
```

The dashboards are displayed in over-sized display panels in command center in bus interchanges.

![DISPLAY](images/dashboard-display.jpg)

## Settings

This application needs a few settings or fields to be configured. The configuration is specified as JSON object and its sample is specified below.

``` javascript
"configuration": {
    "structures": {
      "Rectangle": {
		"x": {
          "description": "The leftmost point where to start the cropping.",
          "type": "integer",
          "default": -1
        },
        "y": {
          "description": "The topmost point where to start the cropping.",
          "type": "integer",
          "default": -1
        },
        "width": {
          "description": "The width of the cropping.",
          "type": "integer",
          "default": -1
        },
        "height": {
          "description": "The height of the cropping.",
          "type": "integer",
          "default": -1
        }
      },
      "ImageThreshold": {
        "useAdaptiveThreshold": {
          "description": "Use adaptive or global threshold.",
          "type": "bool",
          "default": true
        },
        "globalThresholdValue": {
          "description": "Intensity value above which a pixel is considered foreground.",
          "type": "integer",
          "default": 70
        },
        "adaptiveThresholdBlockSize": {
          "description": "Size of a pixel neighborhood that is used to calculate a threshold value for the pixel.",
          "type": "integer",
          "default": 19
        },
        "adaptiveThresholdConstant": {
          "description": "Constant subtracted from the mean or weighted mean.",
          "type": "double",
          "default": 5.0
        }
      },
      "ObjectFilter": {
        "minForegroundPixels": {
          "type": "integer",
          "description": "Minimum number of foreground pixels.",
          "default": 100
        },
        "minAspectRatio": {
          "type": "double",
          "description": "Minimum component_width/component_height aspect ratio.",
          "default": 0.15
        },
        "maxAspectRatio": {
          "type": "double",
          "description": "Maximum component_width/component_height aspect ratio.",
          "default": 0.9
        },
        "minWidthRatio": {
          "type": "double",
          "description": "Minimum component_width/image_width ratio.",
          "default": 0.007
        },
        "maxWidthRatio": {
          "type": "double",
          "description": "Maximum component_width/image_width ratio.",
          "default": 0.075
        },
        "minHeightRatio": {
          "type": "double",
          "description": "Minimum component_height/image_height ratio.",
          "default": 0.0175
        },
        "maxHeightRatio": {
          "type": "double",
          "description": "Maximum component_height/image_height ratio.",
          "default": 0.1
        }
      },
      "PlateDetector": {
        "imageThreshold": {
          "description": "Image threshold configuration.",
          "type": "ImageThreshold"
        },
        "darkOnLightPlate": {
          "description": "Set if the plates to detect are dark foreground on light background or vice versa.",
          "type": "bool",
          "default": true
        },
        "maxStrokeWidthScale": {
          "description": "Set the max width scale of a single stroke, the max width of a stroke will be image_width *  maxStrokeWidthScale",
          "type": "double",
          "default": 0.025
        },
        "objectFilter": {
          "description": "Configuration for filtering the detected object for character recognition.",
          "type": "ObjectFilter"
        },
        "characterMargin": {
          "type": "integer",
          "description": "Margin in pixel between characters in the plate.",
          "default": 3
        },
        "minCharacterPixels": {
          "type": "integer",
          "description": "Minimum number of foreground pixels on each character column.",
          "default": 2
        },
        "minPlateLength": {
          "description": "Minimum length of a plate in characters.",
          "type": "integer",
          "default": 5
        },
        "maxPlateLength": {
          "description": "Maximum length of a plate in characters.",
          "type": "integer",
          "default": 8
        },
        "twoLinePlate": {
          "description": "Detect license plate having two line.",
          "type": "boolean",
          "default": false
        },
        "maxCharactersAngle": {
          "type": "double",
          "description": "Maximum angle (tilt) between characters in degrees.",
          "default": 5
        },
        "maxCharactersWidthDifference": {
          "description": "Maximum difference (percentage) of the width for consecutive characters.",
          "type": "double",
          "default": 0.3
        },
        "maxCharactersHeightDifference": {
          "description": "Maximum difference (percentage) of the height for consecutive characters.",
          "type": "double",
          "default": 0.3
        }
      }
    },
    "inputAddress": {
      "description": "URL of the input video.",
      "type": "string"
    },
    "outputAddress": {
      "description": "Address of the ZMQ PULL socket where to send the output.",
      "type": "string"
    },
    "sendSnapshot": {
      "description": "Send a snapshot of the detected plate along with the recognized plate.",
      "type": "boolean",
      "default": false
    },
    "metadata": {
      "description": "Metadata to send along with the recognized plate.",
      "type": "tuple",
      "default": {}
    },
    "duplicateRetentionTime": {
      "description": "Time (in seconds) to retain a detected plate for avoiding duplicated messages.",
      "type": "integer",
      "default": 60
    },
    "detectorProcesses": {
      "description": "Number of parallel plate detection processes.",
      "type": "integer"
    },
    "cropInput": {
      "description": "Enable cropping of the input frames.",
      "type": "boolean",
      "default": false
    },
    "cropInputArea": {
      "description": "Set the crop area of the input frames when cropping is enabled.",
      "type": "Rectangle"
    },
    "characterDataModel": {
      "description": "Model to use for character recognition.",
      "type": "string",
      "default": "Singapore"
    },
    "plateDetector": {
      "description": "Configuration for the plate detector.",
      "type": "PlateDetector"
    },
    "plateValidator": {
      "description": "Plate validation schema to use.",
      "type": "string",
      "default": "SMRT"
    },
    "testMode": {
      "description": "Run in test mode.",
      "type": "boolean",
      "default": false
    }
```

> **ImageThreshold** section defines setings for adaptive normalization
>
> **PlateDetector** section defines physical characteristics of plate to be detected, for eg. number of characters in plate, two or single line, light color on dark or dark color on light etc.
> 
> **ObjectFilter** section defines the geometry of individual characters and of the plate itself








