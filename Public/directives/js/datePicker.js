/**
 * Created by MohammedSaleem on 12/08/16.
 */

taxiFleetControlPanel.directive('datePicker', function () {
    return {
        restrict: 'A',
        scope: {
            date: "="
        },
        link: function(scope, element, attrs) {
            $(element).datepicker({
                dateFormat: "dd-mm-yy",
                dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
                onSelect: function (date) {
                    scope.date = date;
                    console.log(date)
                    scope.$apply();
                }
            })
        }
    };
});
/*
link: function (scope, element, attrs, ngModelCtrl) {
    element.datepicker({
        /!* dateFormat: "dd-mm-yy",*!/
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        minDate: 0,
        onSelect: function (date) {
            scope.date = date;
            console.log(date)
            scope.$apply();
        }
    });
}*/
