/**
 * Created by MohammedSaleem on 16/06/16.
 */
(function () {

    var jsonFormUI = angular.module('jsonFormUI', []);

    jsonFormUI.controller("jsonFormCont", function ($scope, $http, jsonFormService,appService,sensorService) {
        $scope.getCameraConfiguration=function(cameraName,fieldName){
            getConfig(cameraName.sensorType,cameraName.sensorName,fieldName)
        }
        function getConfig(sensorType,sensorName,fieldName){
            sensorService.getSensorDetailsInstanceMapperByTypeAndSensorName(sensorType,sensorName,fieldName).then(function (result) {
                var fieldValueMapperObj={};
                var fieldValueExist=jsonFormService.getSensorConfig();
                if(fieldValueExist){
                    var resulantObj=MergeRecursive(fieldValueExist, result.data)
                    jsonFormService.setSensorConfig(resulantObj);
                }
                else{
                    jsonFormService.setSensorConfig(result.data);
                    fieldValueMapperObj=jsonFormService.getSensorConfig()
                }

            }, function error(errResponse) {
                console.log("error in getting sensor type")
            });
        }

        function MergeRecursive(obj1, obj2) {

            for (var p in obj2) {
                try {
                    // Property in destination object set; update its value.
                    if ( obj2[p].constructor==Object ) {
                        obj1[p] = MergeRecursive(obj1[p], obj2[p]);

                    } else {
                        obj1[p] = obj2[p];

                    }

                } catch(e) {
                    // Property in destination object not set; create it and set its value.
                    obj1[p] = obj2[p];

                }
            }

            return obj1;
        }

        $scope.extractedData = [];
        $scope.retData={
            "type": "integer"
        };
        $scope.splitCamelCase = function (s) {
            return s.split(/(?=[A-Z])/).join(' ').split('_').join(' ').split('-').join(' ');
        };

        $scope.$on('getFromDataBasedOnInstanceId', function (event, args) {
            console.log("!!!!!!!!!!!!ssss!!!!!!!!!!!!")
            console.log(args.hi)
           /* $scope.extractedData=[];
            var appId=args.currentAppId;
            var url = appService.getRouteUrls();
            appService.getAppDetailsByID(url.routeConfig.apps.appDetailsByID,appId).then(function (result) {
                var descriptionJson=result.data.configuration;
                findAppDescriptionJson(descriptionJson,appId);
            }, function error(errResponse) {
                $scope.appDetailsError = errResponse.data.message
            });*/

        });




        $scope.$on('getFromData', function (event, args) {
            $scope.extractedData=[];
            var appId=args.currentAppId;
            appService.getAppDetailsByID(appId).then(function (result) {

                var descriptionJson=result.data.configuration;
                findAppDescriptionJson(descriptionJson,appId);
            }, function error(errResponse) {
                $scope.appDetailsError = errResponse.data.message
            });

            /*check from with file start*/
          /*  $scope.extractedData=[];
            var appId=args.currentAppId;
            var url = appService.getRouteUrls();
            jsonFormService.getJsonDesc().then(function (result) {
                var descriptionJson=result.data.configuration;
                jsonFormService.getJsonSample().then(function (result) {
                    var sampleAppData=result.data;
                    console.log("************descriptionJson*****************")
                    console.log(descriptionJson)
                    console.log(sampleAppData)
                    var sensorArrayType=[];
                    sensorService.getAllSensorTypes().then(function (res) {

                        for(var l=0;l<res.data.length;l++){
                            sensorArrayType.push(res.data[l].sensorConfiguration.sensorType.typeId)
                        }
                        appService. setAppMapperConfigConstants(sensorArrayType)
                        createForm(descriptionJson, [],sampleAppData,appId)
                    }, function error(errResponse) {
                        console.log("cannot get config")
                    })

                }, function error(errResponse) {
                    $scope.appDetailsError = errResponse.data.message
                });


            }, function error(errResponse) {
                $scope.appDetailsError = errResponse.data.message
            });*/
            /*check from with file end*/

        });

        function findAppDescriptionJson(descriptionJson,appId){
           /* var url = appService.getRouteUrls();*/
            var sensorArrayType=[];
            appService.getAppSampleJson(appId).then(function (result) {
                console.log("**************************@#$@#$**********************************");
                console.log(result.data)
                console.log("**************************@#$@#$**********************************");
                var sampleAppData=result.data;
                sensorService.getAllSensorTypes().then(function (res) {

                    for(var l=0;l<res.data.length;l++){
                        sensorArrayType.push(res.data[l].sensorConfiguration.sensorType.typeId)
                    }
                    appService. setAppMapperConfigConstants(sensorArrayType)
                    createForm(descriptionJson, [],sampleAppData,appId);
                }, function error(errResponse) {
                    console.log("cannot get config")
                })

            }, function error(errResponse) {
                $scope.appDetailsError = errResponse.data.message
            });

        }
        function createForm(descriptionJson, label,sampleAppData,appId) {
         var sensorTypeArrayName=appService.getAppMapperConfigConstants();
         var k = Object.keys(descriptionJson);
         k.forEach(function (objkey, index) {
         if (descriptionJson[k[index]].type||descriptionJson[k[index]].include) {


         label.push(k[index]);
         if( sensorTypeArrayName.indexOf(descriptionJson[k[index]].type)!==-1){
             sensorService.getSensorDetailsListByType(descriptionJson[k[index]].type).then(function (result) {

                 if(descriptionJson[k[index]].default){
                     console.log(descriptionJson[k[index]].default);
                     sampleAppData[label.join('.')]=descriptionJson[k[index]].default;
                 }
                 var dropDownObj={
                     "name": $scope.splitCamelCase(objkey),
                     "realName":objkey,
                     "dropDownValue":result.data,
                     "type": descriptionJson[k[index]].type,
                     "model": sampleAppData[label.join('.')],
                     "isDropDown":true,
                     "description": "select "+" " +descriptionJson[k[index]].type +" from drop down",
                     "isFileType":false

                 }
                 $scope.extractedData.unshift(dropDownObj);
             }, function error(errResponse) {
                 console.log("no sensor are found")
             });

         }
         else if(descriptionJson[k[index]].type==="file"){

             if(descriptionJson[k[index]].default){
                 console.log(descriptionJson[k[index]].default);
                 sampleAppData[label.join('.')]=descriptionJson[k[index]].default;
             }

             var objFile = {
                 "name": $scope.splitCamelCase(objkey),
                 "realName": objkey,
                 "type": descriptionJson[k[index]].type,
                 "description": descriptionJson[k[index]].description,
                 "model": sampleAppData[label.join('.')],
                 "isDropDown":false,
                 "isFileType":true
             };
             if(typeof  sampleAppData[label.join('.')]  === 'object' ){
                 console.log("value is not a object")
                 objFile.model="";
             }
             else{
                 objFile.model=sampleAppData[label.join('.')]
             }
             ($scope.extractedData).push(objFile);

         }
          if((descriptionJson[k[index]].type!=="file")&&(sensorTypeArrayName.indexOf(descriptionJson[k[index]].type)===-1) ){

              if(descriptionJson[k[index]].default){
                  console.log(descriptionJson[k[index]].default);
                  sampleAppData[label.join('.')]=descriptionJson[k[index]].default;
              }

              var obj = {
                  "name": $scope.splitCamelCase(objkey),
                  "realName": objkey,
                  "type": descriptionJson[k[index]].type,
                  "description": descriptionJson[k[index]].description,
                  "model": sampleAppData[label.join('.')],
                  "isDropDown":false,
                  "isFileType":false
              };
              if(typeof  sampleAppData[label.join('.')]  === 'object' ){
                  console.log("value is not a object")
                  obj.model="";
              }
              else{
                  obj.model=sampleAppData[label.join('.')]
              }
              ($scope.extractedData).push(obj);

         }
         label.pop();
         }
         else {
         label.push(k[index]);
             createForm(descriptionJson[k[index]], label,sampleAppData);
         label.pop();
         }
         });

         }



       /* function createForm(descriptionJson, label,sampleAppData,appId) {
            var sensorTypeArrayName=appService.getAppMapperConfigConstants();
            var k = Object.keys(descriptionJson);
            k.forEach(function (objkey, index) {
                if (descriptionJson[k[index]].type||descriptionJson[k[index]].include) {
                    label.push(k[index]);
                    if( sensorTypeArrayName.indexOf(descriptionJson[k[index]].type)!==-1){
                        sensorService.getSensorDetailsListByType(descriptionJson[k[index]].type).then(function (result) {
                            console.log("after call value");
                            console.log(result.data)
                            var dropDownObj={
                              /!*  $scope.splitCamelCase(objkey)*!/
                                "name": $scope.splitCamelCase(objkey),
                                "realName":objkey,
                                "dropDownValue":result.data,
                                "type": descriptionJson[k[index]].type,
                                "model": sampleAppData[label.join('.')],
                                "isDropDown":true,
                                "description": "select "+" " +descriptionJson[k[index]].type +" from drop down",
                                "isFileType":false

                            }
                            $scope.extractedData.unshift(dropDownObj);
                        }, function error(errResponse) {
                            console.log("no sensor are found")
                        });
                    }
                        else if(descriptionJson[k[index]].type==="file"){
                        var objFile = {
                            "name": $scope.splitCamelCase(objkey),
                            "realName": objkey,
                            "type": descriptionJson[k[index]].type,
                            "description": descriptionJson[k[index]].description,
                            "model": sampleAppData[label.join('.')],
                            "isDropDown":false,
                            "isFileType":true
                        };
                        if(typeof  sampleAppData[label.join('.')]  === 'object' ){
                            console.log("value is not a object")
                            objFile.model="";
                        }
                        else{
                            objFile.model=sampleAppData[label.join('.')]
                        }
                        ($scope.extractedData).push(objFile);

                    }
                    else if((sensorTypeArrayName.indexOf(descriptionJson[k[index]].type)!==-1)&&(descriptionJson[k[index]].type==="file") ){
                        var obj = {
                            "name": $scope.splitCamelCase(objkey),
                            "realName": objkey,
                            "type": descriptionJson[k[index]].type,
                            "description": descriptionJson[k[index]].description,
                            "model": sampleAppData[label.join('.')],
                            "isDropDown":false,
                            "isFileType":false
                        };
                        if(typeof  sampleAppData[label.join('.')]  === 'object' ){
                            console.log("value is not a object")
                            obj.model="";
                        }
                        else{
                            obj.model=sampleAppData[label.join('.')]
                        }
                        ($scope.extractedData).push(obj);
                    }
                    label.pop();
                }
                else {
                    label.push(k[index]);
                    findAndPrint(descriptionJson[k[index]], label,sampleAppData);
                    label.pop();

                }

            });

        }*/

    });

    jsonFormUI.directive('jsonForm', ['$http', function ($http) {
        return {
            templateUrl: 'directives/templates/jsonForm.html',
            controller: "jsonFormCont",
            scope: {
                descData: "=",
                sampleData: "=",
                extractedData: "=",
                ret: '&'
            }
        }
    }])


})();