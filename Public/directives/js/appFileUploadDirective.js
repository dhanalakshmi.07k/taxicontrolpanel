/**
 * Created by zendynamix on 13-06-2016.
 */
taxiFleetControlPanel.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                var fileName=element.val().trim();
                $(this).parent().find(".mask").text(fileName);
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });

            });
        }
    };
}]);

