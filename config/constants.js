/**
 * Created by zendynamix on 7/4/2016.
 */

var constants = {
    ASSET_DETAILS:{
        DEFAULT_ASSET_NAME:'car',
        ASSET_NAME_CAR:'car',
        ASSET_NAME_OBD_DEVICE:'obdDevice',
        ASSET_NAME_RFID:'rfIdTag',
        ASSET_NAME_DRIVER:'driver'
    }
}

module.exports={constants:constants};