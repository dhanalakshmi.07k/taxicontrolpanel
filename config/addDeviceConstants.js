/**
 * Created by zendynamix on 8/12/2016.
 */

var gpsDeviceWoxConstants = {
    object_owner:"admin@yourdomain.com",
    icon_id:1,
    group_id:0,
    fuel_measurement_id:1,
    fuel_quantity:1,
    min_moving_speed: 6,
    min_fuel_fillings: 10,
    min_fuel_thefts: 10,
    tail_length: 5,
    timezone_id:0
}

module.exports=gpsDeviceWoxConstants;