var path = require('path'),
  rootPath = path.normalize(__dirname + '/..'),
  env = process.env.NODE_ENV || 'development';

  dbHost=process.env.DB_HOST_NAME;
  dbPort=process.env.DB_PORT;
  apiBaseUrl = process.env.API_BASE_URL;
  notificationRecHost = process.env.NOTIFICATION_RECEIVE_HOST;
  notificationRecPort = process.env.NOTIFICATION_RECEIVE_PORT;

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    port: 5200,
     db: 'mongodb://127.0.0.1:27017/taxiFleetCP-development',
    //db: 'mongodb://163.172.131.83:28018/Nec_ControlPanel_DataBase',
    socketUrl:"http://212.47.249.75:5000",
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    hostName:"is-ext1.nec-labs.com",
    zmq:{
      sendHost:"127.0.0.1",
      recHost:"is-ext1.nec-labs.com",
      notification:'3002'
    }
  },
  integration: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    db: 'mongodb://163.172.131.83:28018/taxiFleetCP',
    /*db:"mongodb://nec-video-cp:necVideoCp@ds011261.mlab.com:11261/nec-video-vp",*/
    port: 5200,
    socketUrl:"http://212.47.249.75:5000",
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    zmq:{
      sendHost:"212.47.249.75",
      recHost:"is-ext1.nec-labs.com",
      port:'5201',
      notification:'3002'
    }
  },
  test: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    port: 5200,
    db: 'mongodb://163.172.131.83:28018/Nec_ControlPanel_DataBase',
   /* db:"mongodb://nec-video-cp:necVideoCp@ds011261.mlab.com:11261/nec-video-vp",*/
    socketUrl:"http://212.47.249.75:5000",
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    zmq:{
      sendHost:"127.0.0.1",
      recHost:"*",
      notification:'5202'
    }
  },
  production: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    db: 'mongodb://163.172.131.83:28018/taxiFleetCP-production',
    /*db:"mongodb://nec-video-cp:necVideoCp@ds011261.mlab.com:11261/nec-video-vp",*/
    port: 5200,
    baseUrl:"http://is-ext1.nec-labs.com:3000",
    zmq:{
      sendHost:"127.0.0.1",
      recHost:"*",
      notification:'5202'
    }
  },

  docker: {
    root: rootPath,
    app: {
      name: 'taxiFleetControlPanel'
    },
    db: 'mongodb://'+dbHost+':'+dbPort+'/taxiFleetCP-Docker',
    /*db:"mongodb://nec-video-cp:necVideoCp@ds011261.mlab.com:11261/nec-video-vp",*/
    port: 5200,
    baseUrl:apiBaseUrl,
    zmq:{
      sendHost:"127.0.0.1",
      recHost:notificationRecHost,
      notification:notificationRecPort
    }
  }

};

module.exports = config[env];
