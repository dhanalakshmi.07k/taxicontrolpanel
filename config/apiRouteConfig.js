/**
 * Created by zendynamix on 22-07-2016.
 */
var config=require('../config/config');
var baseUrl=config.baseUrl;
var routeApiConfig = {
    appApiUrl:"/apps",
    instanceApiUrl:"/instances",
    nodeApiUrl:"/nodes",
    baseUrl:baseUrl
}

module.exports=routeApiConfig;
