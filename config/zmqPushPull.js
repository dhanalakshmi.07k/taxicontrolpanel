/**
 * Created by rranjan on 11/29/15.
 */
var dataHandlers = require("../app/dataHandlers");
var zmq = require('zmq');
var config = require('./config.js')

var zmqPushPull = function (config) {
    startPulling("notification");
    function startPulling(portName) {
        var zmqSocket = zmq.socket('sub');

        var zmqPortPart = 'tcp://' + config.zmq.recHost + ':';

        zmqSocket.connect(zmqPortPart + config.zmq[portName]);
        zmqSocket.subscribe('');

        /*
        zmqSocket.bindSync(zmqPortPart + config.zmq[portName], function (err) {
            if (err)console.log(err.stack)
        });
        */
        console.log("adding a listner " + zmqPortPart + config.zmq[portName]);
        zmqSocket.on("message", function (message) {
            console.log("message .... Notification Arrived")
            try{
                var jsonPayload = JSON.parse(message.toString());
                processData(jsonPayload, portName);
            }catch(err){
                console.error("Error while processing the message :")
                if(message){
                    console.log(message)
                }
            }
        })
    }

    function processData(message, portName) {
        var handlerObjectName =portName+"Handler";
        var handler = dataHandlers[handlerObjectName];
        handler.handler(message);
    }
};


module.exports = {zmqPushPull: zmqPushPull};






