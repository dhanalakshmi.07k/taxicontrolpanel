# Taxi Fleet Control Panel #

## Run application using source code


###Prerequisites 

|Software|Version|
|---|---|
|mongodb|v 3.0.3|
|nodejs|v 4.4.7|
|zmq|v 4.2.1|
|git|v 2.9.X|

### Download Source Code
	git clone <repo>
	
### Configure Application

**Create a new environment if needed. Add a new environment configuration in config.js as shown below:**
> in app/config/config.js

```js

 <EnvironmentName>: {
		root: rootPath,
		app: {
			name: <name of the application> // eg:'taxiFleetControlPanel'
		},
		port: <port number where the application has to  run>, //eg:5200
		baseUrl: <url where the application api  points to>, //eg:http://zendynamix-labs.com:3000
		db: <mongodb url where the application  has to points>, //eg:'mongodb://Test:test@ds011268.mongolab.com:11268/taxiFleetControlPanel'
		zmq:{
			sendHost: <Ip address of the server>,   //eg:"212.47.249.75",
			recHost:"*",
			examplePortName1: <ZMQ port number of examplePortName1> //eg:port:'5201'

 		}
}

```

**_Sample Configuration_**


```js

development: {
	root: rootPath,
	app: {
		name: 'taxiFleetControlPanel'
	},
	port: 5200,
	db: 'mongodb://localhost/taxiFleetCp-development',
	baseUrl:'http://zendynamix-labs.com:3000',
	zmq:{
		sendHost:"127.0.0.1",
		recHost:"*",
		port:'5200',
		notification:'5202'  
	}
}
```
### Install Application  

*  Navigate to the folder where application is present and type the command 
       - npm install
*  Please check config.js which is in config folder of the  application to change the configuration of the following :
     - Database where the application has to point
     - ZMQ port numbers from where the application has to receive the data
     - Url of the application api

### Run Application 
**_Finally to run the application navigate to the application folder and type the command_**

    NODE_ENV=  <ConfigurationName> node app.js 

**_Run using sample Configuration_**
    
    NODE_ENV= development node app.js


##Run application using Docker (Not using prebuilt image)

###Prerequisites 

|  Software |  Version |
|---|---|
| Docker 			 |	v 1.11  |
|  Docker-compose | v 1.8 	 |
| 	git 			|  V 2.9.X |

### Download Source Code
	git clone <repo>

### Configure

**_Edit docker-compose.yml in Application root folder_** 

 Verify and edit if necessary, set correct values for these parameter 

      - API_BASE_URL=http://zendynamix-labs.com:3000
      - NOTIFICATION_RECEIVE_HOST=is-zendynamix-labs.com
      - NOTIFICATION_RECEIVE_PORT=3002

      ports:
      	- "5000:5200"    # web application Port. 5000 is the port on host it will bind to.

      volumes:
    	- ~/data/mongo/cp/:/data/db

  
### Run

**_Use docker-compose to run the application._** 

Please use docker-compose parameter as necessary.

	docker-compose up 
	
This will start 2 linked docker containers, 1 for mongodb and 1 for application.