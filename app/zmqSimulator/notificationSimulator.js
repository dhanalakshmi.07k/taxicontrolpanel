/**
 * Created by SUhas on 7/8/2016.
 */
var zmq = require('zmq')
var zmqPortPart = 'tcp://212.47.249.75:5202'/*http://212.47.249.75/*//*192.168.1.152*/
var sock = zmq.socket('push');
sock.connect(zmqPortPart )
var notificationArray = [
    {
        "level":"WARNING",
        "message": "",
        "timeStamp":new Date(),
        "notificationType":"Application"
    },{
        "level":"INFO",
        "message": "Sensor Configuration Created",
        "timeStamp":new Date(),
        "notificationType":"Instance"
    },{
        "level":"ERROR",
        "message": "Sensor Configuration Deleted",
        "timeStamp":new Date(),
        "notificationType":"Sensor"
    },{
        "level":"WARNING",
        "message": "Generated device ID",
        "timeStamp":new Date(),
        "notificationType":"String"
    }
]
function getPort(data){
    sock.send(data)
}
setInterval(function(){
    console.log("Pushing Notification data")
    var notificationObj = notificationArray[Math.floor(Math.random()*notificationArray.length)];
    getPort(JSON.stringify(notificationObj))
},2000)