/**
 * Created by Suhas on 7/5/2016.
 */
var mongoose = require('mongoose'),
        config = require('../../config/config');
var sensorConfigModel = require('../models/sensorConfiguration');
var argv = process.argv;
if(argv.indexOf('-force')>=0){
    removePreviousData();
}
var fs = require('fs');
var path  =require('path');
var dirPath = fs.realpathSync(__dirname);
var count=0;
//mongoose.connect(config.db);
var sensorConfiguration = [
    {
        "sensorConfiguration" : {

            "instanceDataMapper" : {
            },
            "sensorType" :{
                "typeId":"car",
                "label":"Car"
            },
            "configuration" : {
                "carNo" : {
                    "type" : "text",
                    "description" : "Enter Car Number",
                    "label":"Car Number",
                    "required":true,
                    "autoComplete":false,
                    "assetName":true
                },"fleetNo" : {
                    "type" : "text",
                    "description" : "Enter Fleet Number",
                    "label":"Fleet Number",
                    "required":false,
                    "autoComplete":false
                },
                "make" : {
                    "type" : "dropDown",
                    "description" : "Enter Manufacturer Name",
                    "label":"Make",
                    "required":false,
                    "autoComplete":true,
                    value:{
                        dropDownValues:["Kia","Lexus","Nissan","Toyota","Volvo"],
                        defaultValue:'Kia'
                    }
                },
                "modelNo" : {
                    "type" : "dropDown",
                    "description" : "Enter Model Number",
                    "label":"Model Number",
                    "required":false,
                    "autoComplete":true,
                    value:{
                        dropDownValues:["4Runner","Avalon","Camry","Civilian","ES",
                                        "GS","Land Cruiser","LS","Maxima","Patrol","Sentra",
                                        "Sequoia","Sienna","URvan","V40"],
                        defaultValue:'4Runner'
                    }
                },
                "registrationNumber" : {
                    "type" : "text",
                    "description" : "Enter Registration Number",
                    "label":"Registration Number",
                    "required":false,
                    "autoComplete":false
                },
                "vinNo" : {
                    "type" : "text",
                    "description" : "Enter Vin Number",
                    "label":"Vin Number",
                    "required":false,
                    "autoComplete":false
                },
                "dutyType" : {
                    "type" : "dropDown",
                    "description" : "Enter Duty Type",
                    "label":"Duty Type",
                    "required":false,
                    value:{
                        dropDownValues: ["Hatta Taxi","Ladies and Family","Limousine","Off-road","Public Taxi","Special Needs"],
                        defaultValue:'Hatta Taxi'
                    },
                    "autoComplete":false
                },
                "year" : {
                    "type" : "Number",
                    "description" : "Enter Manufactured Year",
                    "label":"Year",
                    "required":false,
                    "autoComplete":false
                }
            }
        }
    },
    {
        "sensorConfiguration" : {
            "sensorType" :{
                "typeId":"driver",
                "label":"Driver"
            },
            "configuration" : {
                "driverId" : {
                    "type" : "text",
                    "assetName":true,
                    "description" : "Enter Driver Id",
                    "label":"Driver Id",
                    "required":false,
                    "autoComplete":true
                },
                "badgeNo" : {
                    "type" : "text",
                    "assetName":true,
                    "description" : "Enter Badge Number",
                    "label":"Badge Number",
                    "required":false,
                    "autoComplete":true
                },
                "rfid" : {
                    "type" : "text",
                    "assetName":true,
                    "description" : "Enter RFID",
                    "label":"RFID",
                    "required":false,
                    "autoComplete":true
                },
                "driverName" : {
                    "type" : "text",
                    "assetName":true,
                    "description" : "Enter Driver Name",
                    "label":"Driver Name",
                    "required":true,
                    "autoComplete":true
                },
                "nationality" : {
                    "type" : "text",
                    "description" : "Enter Nationality",
                    "label":"Nationality",
                    "required":false,
                    "autoComplete":true
                },
                "gender" : {
                    "type" : "dropDown",
                    "description" : "Enter Driver Name",
                    "label":"Gender",
                    "required":false,
                    "autoComplete":true,
                    value:{
                        dropDownValues: ["Male","Female"],
                        defaultValue:'Male'
                    }
                },
                "department" : {
                    "type" : "text",
                    "description" : "Enter Department",
                    "label":"Department",
                    "required":false,
                    "autoComplete":true
                },
                "jdeDepartment" : {
                    "type" : "text",
                    "description" : "Enter JDE Department",
                    "label":"JDE Department",
                    "required":false,
                    "autoComplete":true
                },
                "dateOfJoin" : {
                    "type" : "date",
                    "description" : "Enter Date Of Join",
                    "label":"Date Of Join",
                    "required":false,
                    "autoComplete":true
                },
                "accommodation" : {
                    "type" : "text",
                    "description" : "Enter Your Accommodation",
                    "label":"Accommodation",
                    "required":false,
                    "autoComplete":true
                },
                "licenceNo" : {
                    "type" : "text",
                    "description" : "Enter Licence No",
                    "label":"Licence No",
                    "required":false,
                    "autoComplete":true
                },
                "mobileNo" : {
                    "type" : "number",
                    "description" : "Enter Mobile No",
                    "label":"Mobile No",
                    "required":false,
                    "autoComplete":true
                }
            },
            "instanceDataMapper":{
            }

        }
    },
    {
        "sensorConfiguration" : {
            "sensorType" :{
                "typeId":"rfIdTag",
                "label":"RFID Tag"
            },
            "configuration" : {
                "make" : {
                    "type" : "text",
                    "description" : "Enter Manufacturer Name",
                    "label":"Make",
                    "required":false,
                    "autoComplete":true
                },
                "modelNo" : {
                    "type" : "text",
                    "description" : "Enter Model Number",
                    "label":"Model Number",
                    "required":false,
                    "autoComplete":true
                },
                "type" : {
                    "type" : "dropDown",
                    "description" : "Enter Model Number",
                    "label":"Model Number",
                    "required":false,
                    value:{
                        dropDownValues:['Active','Passive'],
                        defaultValue:'Active'
                    },
                    "autoComplete":false
                },
                "uniqueIdentifier" : {
                    "type" : "text",
                    "description" : "Enter Unique Identifier",
                    "label":"Unique Identifier",
                    "required":true,
                    "autoComplete":false,
                    "assetName":true
                }
            },
            "instanceDataMapper":{
            }

        }
    },
    {
        "sensorConfiguration" : {
            "sensorType" :{
                "typeId":"obdDevice",
                "label":"Obd Device"
            },
            "configuration" : {
                "make" : {
                    "type" : "text",
                    "description" : "Enter Manufacturer Name",
                    "label":"Make",
                    "required":false,
                    "autoComplete":true
                },
                "modelNo" : {
                    "type" : "text",
                    "description" : "Enter Model Number",
                    "label":"Model Number",
                    "required":false,
                    "autoComplete":true
                },
                "uniqueIdentifier" : {
                    "type" : "text",
                    "description" : "Unique Identifier",
                    "label":"IMEI Number / UId",
                    "required":true,
                    "autoComplete":false
                },
                "gsmId" : {
                    "type" : "text",
                    "description" : "Enter GSM Id",
                    "label":"GSM Id",
                    "required":false,
                    "autoComplete":false,
                    "assetName":true
                }
            },
            "instanceDataMapper":{
            }

        }
    }
]
function generateSensorConfiguration(callBack){
    var sensorConfigJsonPath = path.join(dirPath, "../","data/sensorConfiguration.json")
    /*var sensorConfiguration = JSON.parse(fs.readFileSync(sensorConfigJsonPath));*/
    for(var i=0;i<sensorConfiguration.length;i++){
        var sensorConfigJsonObj = sensorConfiguration[i];
        var sensorConfigObj = new sensorConfigModel();
        sensorConfigObj.sensorConfiguration={};
        sensorConfigObj.sensorType=sensorConfigJsonObj.sensorType;
        sensorConfigObj.sensorConfiguration=sensorConfigJsonObj.sensorConfiguration;
        sensorConfigObj.save(function(err,reult){
            if(err){
                console.log(err)
            }else{
                console.log("Data Added For SensorConfiguration")
                count+=1;
                stopExecution(sensorConfiguration,callBack);
            }

        })
    }

};
function stopExecution(arrayLength,callBack){
    if(count==arrayLength.length){
        console.log(" Sensors Configurations of "+ count +" Sensors saved Successfully");
        callBack();
    }
}
function removePreviousData(callBack){
    console.log("Deleting previous Data")
    sensorConfigModel.remove({},function(err,result){
        console.log("Deleting previous Data")
        generateSensorConfiguration(callBack);
    })
}

module.exports ={
    run:removePreviousData
}

