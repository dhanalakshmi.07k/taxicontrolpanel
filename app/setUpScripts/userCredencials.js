/**
 * Created by zendynamix on 07-03-2016.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config');
var PassportLocalUser = require('../models/PassportLocalUsers');
Schema = mongoose.Schema;
//mongoose.connect(config.db);



function generateUserCredentials(callBack){
    PassportLocalUser.findOne({"username": 'admin'}, function(err,result){
        if(result) {
            console.log("Admin Account Already Present");
            callBack()
        }else{
            var userObj = new PassportLocalUser();
            userObj.username="admin";
            userObj.password="$2a$10$7fu8phzzG3o7XdaVb6xZX.Ac4xuBkDb3zPFBNmxmgW96QUeWDLv2C";
            userObj.email="admin@gmail.com";
            userObj.firstName="admin";
            userObj.lastName="admin";
            userObj.isAdmin=true;
            userObj.save(function(err,result){
                if(err)
                    console.log(err)
                console.log("Credencials generated");
                callBack();
            })
        }

    });

};

module.exports ={
    run:generateUserCredentials
}




