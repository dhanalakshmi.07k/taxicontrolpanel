/**
 * Created by Suhas on 6/18/2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    SensorsModel = mongoose.model('sensors');
var constants = require('../../config/constants').constants;
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
var sensorNotificationPusher = require('../dataManagement/notification').pushToSocket;
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var expressJwt = require('express-jwt'); //https://npmjs.org/package/express-jwt
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};

router.use('/api', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});


router.get('/api/sensors', function (req, res) {
    SensorsModel.find({}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})
router.post('/api/sensors', function (req, res) {
    var sensorsModel = new SensorsModel();
    sensorsModel.createdDate = new Date();
    sensorsModel.sensorData = req.body;
    sensorsModel.save(function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {
            var addedMsg={
                "level":"INFO",
                "message": "Asset Configuration "+sensorsModel.sensorData.assetName+" Created",
                "timeStamp":new Date(),
                "notificationType":"Asset"
            }
            pushSensorNotification(addedMsg);
            res.send(result);
        }
    })
})
router.put('/api/sensors', function (req, res) {
    var assetObj = req.body;
    updateAssetDetails(assetObj).then(function(updatedAssetObj){
        res.send(updatedAssetObj)
    })


})
router.get('/api/sensors/:id', function (req, res) {
    SensorsModel.findOne({'_id': req.params.id}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})


router.delete('/api/sensors/:id', function (req, res) {
    SensorsModel.findOne({'_id': req.params.id},function(errr,response){
        if(errr){
            console.log(errr.stack)
        }else{
            SensorsModel.remove({'_id': req.params.id}, function (err, result) {
                if (err) {

                } else {
                    var deleteMsg={
                        "level":"WARNING",
                        "message": "Asset Configuration '"+response.sensorData.assetName+"'  Deleted",
                        "timeStamp":new Date(),
                        "notificationType":"Sensor"
                    }
                    pushSensorNotification(deleteMsg);
                    res.send(result)
                }
            })
        }
    })
})
router.get('/api/sensorsCount', function (req, res) {
    SensorsModel.count({}, function (err,sensorCount) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var count = {
                count:sensorCount
            }
            res.send(count)
        }
    })

})

router.get('/api/sensorsDetails/instanceDataMapper/:type/:sensorName',function(req,res){
    sensorsConfiguration.findOne({"sensorConfiguration.sensorType.typeId":req.params.type},function (err,sensorConfigurationResponse) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var sensorConfig = sensorConfigurationResponse;
            SensorsModel.findOne({"sensorData.sensorName":req.params.sensorName}, function (err, sensorDataResponse) {
                if (err) {
                    res.send(err.stack)
                }
                else {
                    var count=0;
                    var instanceDataMapper={}
                    var sensorDetail = sensorDataResponse;
                    if(sensorDataResponse && sensorConfig.sensorConfiguration.instanceDataMapper){
                        var instanceDataMapperObj= sensorConfig.sensorConfiguration.instanceDataMapper;
                        var k = Object.keys(instanceDataMapperObj);
                        k.forEach(function (objKey, index) {
                            var objPath = instanceDataMapperObj[objKey].split(".");
                            instanceDataMapper[objKey]=sensorDetail[objPath[0]][objPath[1]];
                            count++;
                            if(k.length==count){
                                res.send(instanceDataMapper)
                            }

                        });
                    }else{
                        res.send([])
                    }
                }
            })
        }
    })

})
router.get('/api/sensorsDetails/:type', function (req, res) {
    SensorsModel.aggregate(
        [
         {$match: {"sensorData.sensorType":req.params.type}},
         {$project: {"_id":0,"sensorData.sensorType":1,"sensorData.sensorName":1}},
         {$project: {"sensorType":"$sensorData.sensorType","sensorName":"$sensorData.sensorName"}}
        ]
        ,function (err, sensorDataResponse) {
            if (err) {
                res.send(err.stack)
            }
            else {
                res.send(sensorDataResponse)
            }
        }
    )
})

function dss(){
}
router.get('/api/sensorsDetails/instanceDataMapper/:type/:sensorName/:fieldName',function(req,res){
    sensorsConfiguration.findOne({"sensorConfiguration.sensorType.typeId":req.params.type},function (err,sensorConfigurationResponse) {
        if (err) {
            res.send(err.stack)
        }
        else {
            var sensorConfig = sensorConfigurationResponse;
            SensorsModel.findOne({"sensorData.sensorName":req.params.sensorName}, function (err, sensorDataResponse) {
                if (err) {
                    res.send(err.stack)
                }
                else {
                    var instanceDataMapper={}
                    var sensorDetail = sensorDataResponse;
                    if(sensorDataResponse && sensorConfig.sensorConfiguration.instanceDataMapper){
                        var instanceDataMapperObj= sensorConfig.sensorConfiguration.instanceDataMapper;
                        if(instanceDataMapperObj[req.params.fieldName]){
                            var objPath = instanceDataMapperObj[req.params.fieldName].split(".");
                            instanceDataMapper[req.params.fieldName]=sensorDetail[objPath[0]][objPath[1]];
                            res.send(instanceDataMapper)

                        }else{
                            res.send({})
                        }
                    }else{
                        res.send([])
                    }
                }
            })
        }
    })

})
router.get('/api/sensorsDetails/isUnique/:sensorName',function(req,res){
    SensorsModel.find({"sensorData.sensorName":req.params.sensorName},function (err,result) {
        if (err) {
            res.send(err.stack)
        }
        else {
            if(result.length>0){
                res.send(false)
            }else{
                res.send(true)
            }
        }
    })

})
function pushSensorNotification(data){
    sensorNotificationPusher(data)
}
function notificationSocketTest(){
    setInterval(function(){
        var testMsg={
            "level":"WARNING",
            "message": "Sensor Configuration 'Test11'  Deleted",
            "timeStamp":new Date(),
            "notificationType":"Sensor"
        }
        pushSensorNotification(testMsg)
    },2000)
}


/*For Pagination*/
router.get('/api/sensors/:skip/:limit/:type', function (req, res) {
    SensorsModel.find({"sensorData.sensorType":req.params.type}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    }).sort({"_id":-1}).skip(parseInt(req.params.skip)).limit(parseInt(req.params.limit));
});
router.get('/api/sensorsCount/:type', function (req, res) {
    SensorsModel.count({"sensorData.sensorType":req.params.type}, function (err, count) {
        if (err) {
            console.log(err.stack)
        } else {
            var count ={
                count:count
            }
            res.send(count)
        }
    });
})
router.get('/api/sensors/:skip/:limit', function (req, res) {
    SensorsModel.find({}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    }).sort({"_id":-1}).skip(parseInt(req.params.skip)).limit(parseInt(req.params.limit));
});


router.get('/api/sensorsData/:type', function (req, res) {
    SensorsModel.find({"sensorData.sensorType":req.params.type}, function (err,result) {
        if (err) {
            console.log(err.stack)
        } else {
            res.send(result)
        }
    });
})

router.put('/api/deLinkAssets', function (req, res) {
    var linkAssetObj = req.body;
    var assetToBeDeLinkedObj = req.body.assetToBeDeLinkedObj;
    assetToBeDeLinkedObj.assetReference=mongoose.Types.ObjectId(assetToBeDeLinkedObj.id);
    SensorsModel.findOne({'_id': linkAssetObj.assetLinkedTo}, function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {
            var assetObj = result;
            if(assetObj.sensorData.assetsLinked){
                if(assetObj.sensorData.assetsLinked[assetToBeDeLinkedObj.type]){
                    var assetsLinked = assetObj.sensorData.assetsLinked[assetToBeDeLinkedObj.type];
                    for(var i=0;i<assetsLinked.length;i++){
                        var assetPresent = assetsLinked[i];
                        if(assetPresent.id===assetToBeDeLinkedObj.id){
                            assetsLinked.splice(i,1)
                            updateAssetDetails(assetObj).then(function(updatedAssetObj){
                                res.send(updatedAssetObj)
                            })
                        }
                    }
                }
            }
        }
    })
    SensorsModel.findOne({'_id': assetToBeDeLinkedObj.id}, function (err, result) {
        if (err) {
            console.log(err.stack)
        }
        else {
            result.sensorData.islinked=false;
                updateAssetDetails(result).then(function(updatedAssetObj){
                res.send(updatedAssetObj)
            })
            }

    })


});

function updateAssetDetails(assetDetails){
    return new Promise(function(resolve,reject){
        assetDetails.UpdatedDate = new Date();
        SensorsModel.findOneAndUpdate({_id: assetDetails._id},assetDetails, function (err, response) {
            if (err) {
                reject(err.stack)
            }
            else {
                var updateMsg={
                    "level":"INFO",
                    "message": "Asset Configuration "+assetDetails.sensorData.assetName+" Updated",
                    "timeStamp":new Date(),
                    "notificationType":"Asset"
                }
                pushSensorNotification(updateMsg);
                resolve(assetDetails)
            }
        })
    })
}


router.get('/api/linkedAssets/:skip/:limit/:assetType', function (req, res) {
    SensorsModel.find({'sensorData.sensorType':req.params.assetType,'sensorData.islinked':true})
            .sort({"_id":-1})
            .skip(parseInt(req.params.skip))
            .limit(parseInt(req.params.limit))
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_DRIVER+'.assetReference')
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_OBD_DEVICE+'.assetReference')
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_RFID+'.assetReference')
            .exec(function(err,result){
                console.log(result);
                res.send(result);
            })
});
router.get('/getData',function(req,res){
    SensorsModel.find({'sensorData.sensorType':'car','sensorData.islinked':true})
            .sort({"_id":1})
            .skip(parseInt(req.params.skip))
            .limit(parseInt(req.params.limit))
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_DRIVER+'.assetReference')
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_OBD_DEVICE+'.assetReference')
            .populate('sensorData.assetsLinked.'+constants.ASSET_DETAILS.ASSET_NAME_RFID+'.assetReference')
            .exec(function(err,result){
                console.log(result);
                res.send(result);
            })
})
router.get('/api/linkedAssetsCounts/:assetType', function (req, res) {
    SensorsModel.count({'sensorData.sensorType':req.params.assetType,"sensorData.isLinked":true}, function (err, count) {
        if (err) {
            console.log(err.stack)
        } else {
            var count ={
                count:count
            }
            res.send(count)
        }
    });
})

/*de-linking assets*/

router.put('/api/linkAssets', function (req, res) {
    var linkAssetObj = req.body;
    var assetToBeLinkedObj = req.body.assetToBeLinked;
    assetToBeLinkedObj.assetReference=mongoose.Types.ObjectId(assetToBeLinkedObj.id);
    SensorsModel.findOne({'_id': linkAssetObj.assetLinkedTo}, function (err, result) {
        if (err) {
            console.log(err.stack)
        } else {
            var assetObj = result;
            if(assetObj.sensorData.assetsLinked){
                if(assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type]){
                    var assetsLinked = assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type]
                    var assetPresentFlag = false;
                    if(assetsLinked.length<=0){
                        assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type]=[];
                        assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)
                    }else if(assetsLinked.length>0){
                        assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)
                    }
                    /*for(var i=0;i<assetsLinked.length;i++){
                        var assetPresent = assetsLinked[i];
                        if(assetPresent.id===assetToBeLinkedObj.id){
                            assetPresentFlag = true;
                        }
                        if(assetsLinked.length-1==i && !assetPresentFlag){
                            assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)
                        }else{
                            break;
                        }
                    }*/
                    /*assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)*/
                    /*assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)*/
                }else{
                    assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type]=[];
                    assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)
                }
            }else{
                assetObj.sensorData.assetsLinked = {}
                assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type]=[];
                assetObj.sensorData.assetsLinked[assetToBeLinkedObj.type].push(assetToBeLinkedObj)
            }
            assetObj.sensorData.isLinked = true;
            updateAssetDetails(assetObj).then(function(updatedAssetObj){
                res.send(updatedAssetObj)
            })
        }
    })


});

router.get('/getSensorByUniqueIdentifierNumber/:uniqueIdentifierNumber', function (req, res) {
    SensorsModel.findOne({'sensorData.uniqueIdentifier': req.params.uniqueIdentifierNumber}, function (err, result) {
        if (err) {

        } else {
            res.send(result)
        }
    })
})
router.get('/updatesensorDeviceBYuniqueIdentifierNumber/:uniqueIdentifierNumber/:apiRefId', function (req, res) {
    SensorsModel.update({'sensorData.uniqueIdentifier': req.params.uniqueIdentifierNumber},{ $set: { "sensorData.apiRefId" : req.params.apiRefId} },function (err, result) {
        if (err) {
            console.log("******************")
            console.log(err)
        } else {
            console.log(result)
            res.send(result)
        }
    })

})


router.get('/findSensorDeviceBYUniqueIdentifierNumber/:uniqueId', function (req, res) {
    SensorsModel.findOne({'sensorData.uniqueIdentifier': req.params.uniqueId}, function (err, result) {
        if (err) {
            console.log("******************")
            console.log(err)
        } else {
            console.log(result)
            res.send(result)
        }
    })
})

router.get('/assetDetails/searchByName/:type/:assetName',function(req,res){
    SensorsModel.aggregate([
                {$match: {
                    "sensorData.assetName":{$regex:req.params.assetName, $options: 'i'},
                    "sensorData.sensorType":req.params.type
                }},
                {$skip: 0},
                {$limit: 10},
                {$sort: {"_id":-1}},
                {$project: {"value":{"id":"$_id","assetName":"$sensorData.assetName"},"_id":0}
                }
            ],
            function(err,result){
                if(err){
                    console.log(err)
                }else{
                    res.send(result)
                }
            });

})
