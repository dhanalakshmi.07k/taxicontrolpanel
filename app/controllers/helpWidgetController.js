/**
 * Created by Suhas on 6/20/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config=require('../../config/config'),
    WidgetHelpModel = mongoose.model('helpWidgetDetails');
    module.exports = function (app) {
        app.use('/', router);
    };

router.get('/helpWidgets',function(req,res){
    WidgetHelpModel.find({},function(err,result){
        if(err){

        }else{
            res.send(result)
        }
    })
})
router.post('/helpWidgets',function(req,res){
    var widgetHelpModel = new WidgetHelpModel();
    widgetHelpModel.createdTime = new Date();
    widgetHelpModel.pageCode = req.body.pageCode;
    widgetHelpModel.widgetName = req.body.widgetName;
    widgetHelpModel.widgetDescription = req.body.widgetDescription;
    widgetHelpModel.save(function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send("data Saved")
        }
    })
})
router.put('/helpWidgets',function(req,res){
    req.body.updatedTime = new Date();
    WidgetHelpModel.findOneAndUpdate({_id:req.body._id},req.body,function(err,response){
        if(err){
            res.send(err.stack)
        }
        else{
            res.send("updated SuccessFully")
        }
    })

})
router.delete('/helpWidgets/:id',function(req,res){
    WidgetHelpModel.remove({_id:req.params.id},function(err,response){
        if(err){
            res.send(err.stack)
        }
        else{
            res.send("Removed SuccessFully")
        }
    })

})
router.get('/helpWidgets/content/:pageCode',function(req,res){
    WidgetHelpModel.findOne({"pageCode":req.params.pageCode},'widgetDescription',function(err,result){
        if(err){

        }else{
            res.send(result)
        }
    })
})
router.get('/helpWidgets/isContentAlreadyExist/:pageCode',function(req,res){
    WidgetHelpModel.find({"pageCode":req.params.pageCode},'widgetDescription',function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            if(result.length>0){
                res.send(true)
            }else{
                res.send(false)
            }
        }
    })
})
