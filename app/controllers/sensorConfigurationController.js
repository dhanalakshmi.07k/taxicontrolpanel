/**
 * Created by Suhas on 7/5/2016.
 */
var express = require('express'),
    router = express.Router();
var mongoose = require('mongoose');
var sensorsConfiguration =  mongoose.model('sensorsConfiguration');
module.exports = function (app) {
    app.use('/', router);

};
router.get('/sensorConfiguration/:sensorType',function(req,res){
    sensorsConfiguration.findOne({'sensorConfiguration.sensorType.typeId':req.params.sensorType},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }
    })
});
router.get('/sensorConfiguration/sensorTypes/all',function(req,res){
    sensorsConfiguration.find({},{'sensorConfiguration.sensorType':1},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            res.send(result)
        }
    })
});

router.get('/api/sensorConfiguration/assetNameByType/:assetType',function(req,res){
    sensorsConfiguration.findOne({'sensorConfiguration.sensorType.typeId':req.params.assetType},function(err,result){
        if(err){
            console.log(err.stack)
        }else{
            var data = result.sensorConfiguration.configuration;
            var k = Object.keys(data);
            k.forEach(function (objKey, index) {
                if (data[k[index]].type) {
                    if(data[k[index]].assetName){
                        res.send(k[index])
                    }
                }
            });
        }
    })
});
