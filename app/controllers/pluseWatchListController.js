/**
 * Created by zendynamix on 22-07-2016.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config = require('../../config/config'),
    routeConfig = require('../../config/apiRouteConfig'),
    request = require('request');
    var multiparty = require('multiparty');
    var util = require('util');



var jwt    = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var secret = 'this is the secrete password';
module.exports = function (app) {
    app.use('/', router);
};

router.use('/pulseWatchListApi', expressJwt({secret: secret}));
router.use(function(err, req, res, next){
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).send('Unauthorized user please login via the application');
    }
});

router.get('/pulseWatchList/*', function (req, res) {
    var apiUrl=req.url.substring(15,req.url.length);
    var options = {
        method: 'GET',
        url:config.baseUrl+apiUrl
    };
    request(options, function (error, response, body) {
        if (error){res.send(error) }
        else {
            res.send(body);
        }


    });

})
router.delete('/pulseWatchList/*', function (req, res) {
    var apiUrl=req.url.substring(15,req.url.length);
    var options = {
        method: 'DELETE',
        url:config.baseUrl+apiUrl
    };
    request(options, function (error, response, body) {
        if (error){
            res.send(error) }
        else {
            res.send(body);
        }
    });
})









router.post('/pulseWatchList/:appUrl/:id/:partStringUrl', function (req, res) {
    var apiBaseUrl=req.params.appUrl;
    var statusBody={};
    var requestUrl;
    if(req.body.status){
        statusBody.status=req.body.status;
        requestUrl=config.baseUrl+'/'+apiBaseUrl+'/'+req.params.id+'/'+req.params.partStringUrl
    }else if(req.body.nodes){
        statusBody.nodes=req.body.nodes
        requestUrl=config.baseUrl+routeConfig.instanceApiUrl+'/'+req.params.id+'/'+req.params.partStringUrl;

    }else if(req.body.instanceConfig){
        statusBody=JSON.parse(req.body.instanceConfig);
        requestUrl=config.baseUrl+'/'+apiBaseUrl+'/'+req.params.id+'/'+req.params.partStringUrl
    }
    var options = {
        method: 'POST',
        url: requestUrl,
        body: statusBody,
        json: true
    };

    request(options, function (error, response, body) {
        if (error){
            res.send(error)
        } else{

            res.send(body)
        }

    })


})

router.post('/pulseWatchListPostFile/:appUrl/:id/:partStringUrl', function (req, res,body) {
    var apiBaseUrl=req.params.appUrl;
    var reqUrl=config.baseUrl+'/'+apiBaseUrl+'/'+req.params.id+'/'+req.params.partStringUrl;
    req.pipe(request.post(reqUrl,req.body)).pipe(res);

})

router.post('/pulseWatchList/:appUrl', function (req, res,body) {
    var apiBaseUrl=req.params.appUrl;
    var requestUrl=config.baseUrl+'/'+apiBaseUrl
    if(req.body.appId){
        var options = {
            method: 'POST',
            url: requestUrl,
            body: req.body,
            json: true
        };

        request(options, function (error, response, body) {
            if (error){
                res.send(error)
            } else{

                res.send(body)
            }

        })
    }
    else {
        var apiBaseUrl = req.params.appUrl;
        var reqUrl = config.baseUrl + '/' + apiBaseUrl;
         req.pipe(request.post(reqUrl,req.body)).pipe(res);
       /* req.pipe(request.post(reqUrl, body)).on('response', function (res) {
            console.lo("&&&&&&&&&&&&&&&&&g");
            delete res.headers['Content-Type'];
        }).pipe(res);*/
    }


})





