/**
 * Created by Suhas on 4/4/2016.
 */
var express = require('express'),
        router = express.Router(),
        mongoose = require('mongoose'),
        config=require('../../config/config'),
        SettingsModel = mongoose.model('settingModel');
        appSensorMappingModel= mongoose.model('sensorAppMapper');
        module.exports = function (app) {
        app.use('/', router);
        };
router.get('/necDashBoard/settings/getSocketUrl',function(req,res){
        var url = config['socketUrl'];
        res.send(url);
})





router.post('/appMapperObj', function(req, res, next) {
    var mapperObj = new appSensorMappingModel();
    //camera
    mapperObj.sensorType="camera";
    mapperObj.save(function(err,result){
        if(err){
            console.log(err)
        }
        else{
            console.log("mapperObj generated")
            res.send("mapperObj generated")
        }

    })


});
router.get('/appSensorMappingConfig',function(req,res){
    appSensorMappingModel.find({},{"_id":0},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})
router.get('/appSettings',function(req,res){
    SettingsModel.findOne({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})







