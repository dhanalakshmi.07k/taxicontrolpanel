/**
 * Created by zendynamix on 7/11/2016.
 */

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    config=require('../../config/config'),
    SettingsModel = mongoose.model('settingModel');


    module.exports = function (app) {
        app.use('/', router);
    };

router.get('/configMapDetails',function(req,res){
    SettingsModel.find({},function(err,result){
        if(err){
            res.send(err.stack)
        }else{
            res.send(result)
        }
    })
})



router.post('/config', function(req, res, next) {
    SettingsModel.find({"SettingsConfiguration.id":1},function(err,result){
        console.log( req.body)
        /*console.log(result[0].SettingsConfiguration.mapSettings.mapCenter.latitude)*/
        result[0].SettingsConfiguration.mapSettings.mapCenter.latitude = req.body.centerLatitude;
        result[0].SettingsConfiguration.mapSettings.mapCenter.longitude = req.body.centerLongitude;
        result[0].save(function(err) {
            if (err){
                console.log('Error in Saving configObj: '+err);
            }

            res.send("configObj added sucessfully");
        });
    })



});