/**
 * Created by Suhas on 7/7/2016.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var seedDataSchema = new Schema({
    isSeeded:Boolean,
    code:Number,
    timeStamp:Date
},{collection: "seedData"})

module.exports = mongoose.model('seedData1',seedDataSchema);