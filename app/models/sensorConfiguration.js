/**
 * Created by Suhas on 7/5/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var sensorConfigurationSchema = new mongoose.Schema({sensorConfiguration:{}},{collection:"sensorsConfiguration"});

module.exports = mongoose.model('sensorsConfiguration',sensorConfigurationSchema);