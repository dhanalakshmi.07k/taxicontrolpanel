/**
 * Created by zendynamix on 19-06-2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var helpWidgetSchema = new Schema({
    pageCode:String,
    widgetName: String,
    widgetDescription: String,
    createdTime:Date,
    updatedTime:Date
},{collection: "helpWidgetDetails"})
module.exports = mongoose.model('helpWidgetDetails',helpWidgetSchema);

