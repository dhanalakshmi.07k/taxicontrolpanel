/**
 * Created by Suhas on 7/7/2016.
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var notificationSchema = new Schema({
    level:String,
    message: String,
    timeStamp:Date,
    notificationType:String
},{collection: "notification"})
module.exports = mongoose.model('notification',notificationSchema);