/**
 * Created by Suhas on 4/16/2016.
 */
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var settingSchema = new Schema({
        "SettingsConfiguration":{
                id:Number,
                "mapSettings":{
                        "mapCenter":{
                                "latitude":Number,
                                "longitude":Number
                        }
                },
               uiThemeName:String,
                uiStyleName:String

}
},{collection: "settingModel"})
module.exports = mongoose.model('settingModel',settingSchema);
