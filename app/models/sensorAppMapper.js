/**
 * Created by zendynamix on 7/8/2016.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var sensorAppMapperSchema = new mongoose.Schema(
    {
                "sensorType":String
    },{collection: "sensorAppMapper"})
module.exports = mongoose.model('sensorAppMapper',sensorAppMapperSchema);