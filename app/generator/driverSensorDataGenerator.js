/**
 * Created by Suhas on 8/10/2016.
 */
var fs = require('fs');
var path  =require('path');
var dirPath = fs.realpathSync(__dirname);
var AssetConfigurationModel = require('../models/sensorModel');
var mongoose = require('mongoose'),
        config = require('../../config/config');
mongoose.connect(config.db);
var generateTaxiFleetCarDataCounter = 0;
function generateTaxiFleetDriverData(){
        return new Promise(function(resolve,reject) {
                var driverData = path.join(dirPath, "../", "data/driversData.json");
                var driverAssetConfigurationData = JSON.parse(fs.readFileSync(driverData));
                if (driverAssetConfigurationData && driverAssetConfigurationData.length > 0) {
                        for (var i = 0; i < driverAssetConfigurationData.length; i++) {
                                var driverConfigurationObj = driverAssetConfigurationData[i]
                                var assetConfigurationModelObj = new AssetConfigurationModel();
                                assetConfigurationModelObj.sensorData = {};
                                assetConfigurationModelObj.sensorData = driverConfigurationObj.sensorData;
                                assetConfigurationModelObj.sensorData.assetName = driverConfigurationObj.sensorData["driverName"];
                                assetConfigurationModelObj.save(function (err,result) {
                                        generateTaxiFleetCarDataCounter++
                                        if (err) {
                                                console.log(err.stack)
                                        } else {
                                                /* console.log("result Saved")*/
                                        }
                                        if (driverAssetConfigurationData.length == generateTaxiFleetCarDataCounter) {
                                                resolve();
                                        }
                                })

                        }
                }
        })
}

if(process.argv.indexOf("--force")>=0){
        console.log("All Previous Drivers Details Removed Successfully");
        AssetConfigurationModel.remove({'sensorData.sensorType':'driver'},function(){
                generateTaxiFleetDriverData().then(function(){
                        console.log("All Drivers Details Saved Successfully");
                        process.exit(0);
                })
        })
}
else{
        generateTaxiFleetDriverData().then(function(){
                console.log("All Drivers Details Saved Successfully");
                process.exit(0);
        })
}